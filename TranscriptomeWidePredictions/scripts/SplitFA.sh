#!/bin/bash
#$ -cwd

FA=$1

while read line;do

    if [[ ${line:0:1} == '>' ]];then

    	# filter for CDS containing transcripts
    	if [[ $line == *"|CDS:"* ]]; then
  			name=$(echo $line | awk -F"|" '{gsub(">","",$1);print $1}')	
  		else
  			continue
		fi

		# replace pipes wuth underscores because of naming issues
    	newLine=$(echo $line | tr '|' '_')
        #outfile=${newLine#>}.fasta
        outfile=${name}.fasta
        echo $line > $outfile
    else
        echo $line >> $outfile
    fi
done < $FA