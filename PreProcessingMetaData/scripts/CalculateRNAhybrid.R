#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

# You will need to change the path to your RNAhybrid examples directory amnd executable
EXAMPLEDIR = "~/biotools/RNAhybrid-2.1.2/examples"
RNAhybrid = "~/bin/RNAhybrid"


###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(Biostrings, quietly = T))
suppressMessages(library(Rsamtools, quietly = T))
suppressMessages(library(GenomicAlignments, quietly = T))

###### sub-routines #################################

GetGuide <- function(y){
  return( strsplit(y, split = ":")[[1]][1] )
}


GetType<-function(x){
  if(grepl("FirstOrder",x)){
    return("First Order")
  }
  else if (grepl("consecTriple",x)){
    return("Consecutive Triple")
  }
  else if (grepl("randomDouble",x)){
    return("Random Double")
  }
  else if (grepl("rc_",x)){
    return("Non-Targeting")
  }
  else if (grepl("consecDouble",x)){
    return("Consecutive Double")
  }
  else{
    return("Perfect Match")
  }
}

# All together, it will take ~35hrs

########################## sub-routines end  ########

###### execute ######################################

if (length(args)!=4) {
  stop("Exiting! Please provide 2 arguments: 1) a single file.fasta input that is target of the guide library: <file.fasta>, 2) a bam file with the aligned guide RNAs <library.Inputmatch.bam>, 3) an integer with the guide length <27 or 23> and 4) an output name prefix <string>", call.=FALSE)
}else{
  FA.file = args[1] # "./GFP.fa"
  Bam = args[2] # "GFP_library.Inputmatch.bam"
  GuideLength = as.integer(args[3]) # 27 for GFP lib and 23 for other libs
  OutName = args[4]
}




# Load GFP Fasta File
FA <- Biostrings::readDNAStringSet(filepath = FA.file, format = "fasta", use.names = T)
# Calculate dinucleotide Frequence for the GFP target
diNtFreq <- t(dinucleotideFrequency(FA)/sum(dinucleotideFrequency(FA)))
diNtFreq.out = c("# dinucleotide frequencies from GFP coding reagions",paste0(rownames(diNtFreq),": ",diNtFreq[,1]))



# You will need to change the path to your RNAhybrid examples directory 
write.table(diNtFreq.out, file = paste0(EXAMPLEDIR,"/3UTR_worm.freq"), quote = F, row.names = F, col.names = F)

# collect guide and 'perfect match' target sequence
p1 <- ScanBamParam(what=c("qname","flag","rname","strand","pos","mapq","cigar","seq","qual"),tag=c('MD'), reverseComplement = FALSE)
bam <- readGAlignments(Bam, param=p1)
GuideSeq = reverseComplement(mcols(bam)$seq)  # all guide sequences
names(GuideSeq) = mcols(bam)$qname # names the guides
TargetSeq = mcols(bam)$seq[grep("First|Random|Double|Triple", mcols(bam)$qname, invert = T)] # get the target sequence for all perfect matching guides
names(TargetSeq) = sapply(mcols(bam)$qname[grep("First|Random|Double|Triple", mcols(bam)$qname, invert = T)],GetGuide) # name those
TargetSeq.rev = reverse(TargetSeq)
names(TargetSeq.rev) = names(TargetSeq)



#transform into a data frame
guide.df = as.data.frame(GuideSeq)
colnames(guide.df) = "GuideSeq"
guide.df$ParentGuide <- sapply(rownames(guide.df),GetGuide)
guide.df$Type <- sapply(rownames(guide.df),GetType)
guide.df$TargetSeq <- NA
Target.df <- as.data.frame(TargetSeq)
guide.df$TargetSeq.rev <- NA
Target.rev.df <- as.data.frame(TargetSeq.rev)



idx = match( guide.df$ParentGuide ,  rownames(Target.df))

if(all(guide.df$ParentGuide == rownames(Target.df)[idx])){
  guide.df$TargetSeq <- Target.df$x[idx]
  guide.df$TargetSeq.rev <- Target.rev.df$x[idx]
}



max = GuideLength
col.lbls = vector()
for (i in 1:max){
  width = max-(i-1)
  col.lbls = c(col.lbls,paste0(i,":",seq(1,width,1)))
}



out.ma <- matrix(0, nrow=nrow(guide.df), ncol = length(col.lbls))
colnames(out.ma) <- col.lbls
rownames(out.ma) <- rownames(guide.df)
print(Sys.time())
for (i in 1:nrow(guide.df)){
  
  
  cat(paste0(i," of ",nrow(guide.df),"\n"))
  
  g = guide.df$GuideSeq[i]
  t = guide.df$TargetSeq[i]
  
  for (k in 1:ncol(out.ma)){
    
    window = as.numeric(strsplit(colnames(out.ma)[k], ":")[[1]])
    g.sub = substr(g,start = window[1], stop = (window[1] + window[2]) - 1 )
    t.sub = substr(t,start = (max - (window[1] -1)) - (window[2] - 1), stop = max - (window[1] - 1) )
    
    # You will need to change the path to your RNAhybrid executable 
    cmd = paste0( RNAhybrid ," -c -s 3utr_worm ",g.sub," ",t.sub)
    output = system(cmd , intern = TRUE)
    
    out.ma[i,k] = as.numeric(strsplit(output, split = ":")[[1]][5]) # Gets MFE
    # out.ma[i,k] = as.numeric(strsplit(output, split = ":")[[1]][6]) # Get p_score
    
    if (k == GuideLength){
      cat(paste0(cmd,"\n"))
    }
    
  }
  
  
  
}
print(Sys.time())

guide.df.merged = cbind.data.frame(guide.df,out.ma)
write.table(guide.df.merged , file = paste0(OutName,'_HybridizationMatrix_merged_All.csv'), sep=",", quote = F, col.names = T, row.names = T)
