#!/bin/bash
#$ -l mem_free=8G,h_vmem=12G
#$ -cwd

FASTA=$1 # Cas13d_GFP_library.final.fa
GuideLen=$2
PREFIX="${FASTA%.*}"
SCRIPTS="../scripts"
RNAFOLD="../scripts/RNAfold"
DirectRepeat="aacccctaccaactggtcggggtttgaaac"
UPSTREAMSEQ=""
DOWNSTREAMSEQ=""

module purge
module load fastx-tools/0.0.14
module load samtools


# if you want to add an upstream sequences, e.g. when adding the Cas13d Direct Repeat
# For Cas13d, the Direct Repeat is upstream and needs to be pasted 5' to the reverse complemented crRNA sequence
bash ${SCRIPTS}/AddSequenceToFasta.sh  -i ${FASTA} -u $DirectRepeat > ${PREFIX}.crRNAplusDR.fa

# if you want to add upstream and downstream sequences, e.g. when adding the gibson overhangs
#bash ${SCRIPTS}/AddSequenceToFasta.sh  -i ${PREFIX}.crRNA.fa -u $UPSTREAMSEQ -d $DOWNSTREAMSEQ > ${PREFIX}.crRNAplusOverhangs.fa

# use RNAfold for MFE (minimum free energy) prediction
cat ${PREFIX}.crRNAplusDR.fa | $RNAFOLD --gquad > ${PREFIX}.crRNAplusDR.folded.fa

if [[ "$GuideLen" == "27" ]]; then 
	cat ${PREFIX}.crRNAplusDR.folded.fa | awk '/^>/ { if(i>0) printf("\n"); i++; printf("%s\t",$0); next;} {printf("%s",UPSEQ$0);} END { printf("\n");}' | awk -F"\t" '{gsub("u","t",$2);gsub("U","T",$2)}{print $0}'| awk '{$4=substr($2,1,57);$5=substr($2,58,114);gsub(/\(|\)/,"",$3);printf("%s\n%s\n",$1"_"$3"_"$5,$4)}'  >  ${PREFIX}.crRNAplusDR.foldedTransformed.fa
else
	cat ${PREFIX}.crRNAplusDR.folded.fa | awk '/^>/ { if(i>0) printf("\n"); i++; printf("%s\t",$0); next;} {printf("%s",UPSEQ$0);} END { printf("\n");}' | awk -F"\t" '{gsub("u","t",$2);gsub("U","T",$2)}{print $0}'| awk '{$4=substr($2,1,53);$5=substr($2,54,106);gsub(/\(|\)/,"",$3);printf("%s\n%s\n",$1"_"$3"_"$5,$4)}'  >  ${PREFIX}.crRNAplusDR.foldedTransformed.fa
fi


rm *.ps

exit
