#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(Biostrings, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(ggpubr, quietly = T))




###### sub-routines #################################

GetLetterFreq = function(x = Results, G=GUIDES, S=1, E=27){
  
  G = G[x$GuideName]
  G.sub = subseq( x = G,start = S,end = E)
  G = G.sub
  
  AU.freq <- letterFrequency( G , letters = c("AT") ,as.prob = F)
  GC.freq <- letterFrequency( G , letters = c("GC") ,as.prob = F)
  AU.prob <- letterFrequency( G , letters = c("AT") ,as.prob = T)
  GC.prob <- letterFrequency( G , letters = c("GC") ,as.prob = T)
  A.prob <- letterFrequency( G , letters = c("A") ,as.prob = T)
  C.prob <- letterFrequency( G , letters = c("C") ,as.prob = T)
  G.prob <- letterFrequency( G , letters = c("G") ,as.prob = T)
  U.prob <- letterFrequency( G , letters = c("T") ,as.prob = T)
  A.freq <- letterFrequency( G , letters = c("A") ,as.prob = F)
  C.freq <- letterFrequency( G , letters = c("C") ,as.prob = F)
  G.freq <- letterFrequency( G , letters = c("G") ,as.prob = F)
  U.freq <- letterFrequency( G , letters = c("T") ,as.prob = F)
  diNucleotide.prob <- dinucleotideFrequency(G, as.prob = T)
  diNucleotide.freq <- dinucleotideFrequency(G, as.prob = F)
  
  
  
  out <- cbind.data.frame(A.freq,C.freq,G.freq,U.freq,A.prob,C.prob,G.prob,U.prob,GC.freq,AU.freq,GC.prob,AU.prob,diNucleotide.freq,diNucleotide.prob)
  rownames(out) <- names(G)
  colnames(out) <- c("A","C","G","T","pA","pC","pG","pT","G|C", "A|T", "pG|pC", "pA|pT", "AA","AC","AG","AT","CA","CC","CG","CT","GA","GC",
                     "GG","GT","TA","TC","TG","TT","pAA","pAC","pAG","pAT","pCA","pCC","pCG","pCT","pGA","pGC","pGG","pGT","pTA","pTC","pTG","pTT")
  
  idx = match(rownames(out) , x$GuideName )
  
  if (all(rownames(out) ==  x$GuideName[idx])  ){
    out$MatchType <- x$MatchType[idx]
    out$meanCS <- x$meanCS[idx]
  }
  
  return(out)
}


GetNTfreqType = function(x){
  if (nchar(x) == 1){
    return("nucleotide")
  }
  else if (nchar(x) == 2){
    return("di-nucleotide")
  }
  else if ( grepl("\\|",x) ){
    return("nucleotides")
  }
  else{
    stop("Exiting! unexpected input")
  }
}

lmp <- function (modelobject) {
  if (class(modelobject) != "lm") stop("Not an object of class 'lm' ")
  f <- summary(modelobject)$fstatistic
  p <- pf(f[1],f[2],f[3],lower.tail=F)
  attributes(p) <- NULL
  return(p)
}

LollipopPlot = function( FREQ = LetterFreq.gfp , NAME = ""){
  
  c = cor(FREQ[,grep("MatchType|p",colnames(FREQ), invert = T)],use = "complete.obs")
  df = melt(c["meanCS",grep("meanCS", colnames(c) ,invert = T)])
  rownames(df) <- gsub("\\.","|",rownames(df))
  colnames(df) <- "pearson_coefficient"
  df$Nucleotide <- rownames(df)    
  df$type <- sapply(rownames(df), FUN = GetNTfreqType)
  df$type <- factor( df$type, levels = c("nucleotide","nucleotides","di-nucleotide"))
  df$Nucleotide <- gsub("T","U",df$Nucleotide)
  
  g1 = ggdotchart(df, x = "Nucleotide", y = "pearson_coefficient",
                  color = "type",                               # Color by groups
                  palette = c( "#E7B800", "#FC4E07","#00AFBB"), # Custom color palette
                  sorting = "descending",                       # Sort value in descending order
                  add = "segments",                             # Add segments from y = 0 to dots
                  add.params = list(color = "lightgray", size = 2), # Change segment color and size
                  #group = "type",                               # Order by groups
                  dot.size = 7,                                 # Large dot size
                  label = df$Nucleotide,                        # Add mpg values as dot labels
                  # label = round(df$pearson_coefficient,1),
                  font.label = list(color = "white", size = 9, vjust = 0.5),        # Adjust label parameters
                  ggtheme = theme_pubr(),                        # ggplot2 theme
                  ylim=c(-0.5,0.5),
                  title = NAME)
  
  return(g1)
  
}


GCcorPlot = function(FREQ = LetterFreq.gfp, NAME = ""){
  
  cols = which(colnames(FREQ) == "pG|pC" | colnames(FREQ) == "meanCS")
  dat = FREQ[,cols]
  colnames(dat)[1] = "GC"
  
  fit <- lm(  dat$meanCS ~ dat$GC )
  r = summary(fit)$adj.r.squared
  c = cor(dat , use = "complete.obs" )[2]
  
  Y = sum(abs(range(dat$meanCS , na.rm = T)))
  X = max(dat$GC , na.rm = T) - min(dat$GC , na.rm = T) 
  R = X/Y
  My = max(dat$meanCS , na.rm = T)
  Mx = min(dat$GC , na.rm = T)
  
  g2 = ggplot(dat, aes(x=GC, y=meanCS)) +
    geom_point(shape=20) +    # Use hollow circles
    geom_smooth(method=lm,   se=F, colour="#c82026")  +      
    geom_smooth(se = T, method = "loess", na.rm = T) +
    # geom_smooth(method = lm, formula = y ~ splines::bs(x, 3), se = T) +
    theme_classic() +
    ggtitle(NAME) + 
    ylab("log2(Top/Input)") + xlab("GC content") +
    annotate( geom = "text", label = paste0("rho = ",signif(c,2)), y = My-0.1, x = Mx, hjust = "inward") +
    annotate( geom = "text", label = paste0("R^2 = ",signif(r,2)), y = My-0.4, x = Mx, hjust = "inward") +
    annotate( geom = "text", label = paste0("p = ",signif(lmp(fit),2)), y = My-0.7, x = Mx, hjust = "inward") +  
    coord_fixed(ratio = R) 
  return(g2)
}

########################## sub-routines end  ########

###### execute ######################################


# Read combined result table
Results = read.delim('./data/CombinedTilingScreenResults.csv' , sep = ',', header = T, stringsAsFactors = F)

# Only use CDS annotating guides (can be scwitched off)
Results = Results[grep("CDS",Results$Annotation),]

# Perfect Matches only
Results = Results[which(Results$MatchType == "Perfect Match"),]

# Restrict to single Screens
Screens = split(Results, f = Results$Screen)


GFP = Screens[["GFP"]]
CD46 = Screens[["CD46"]]
CD55 = Screens[["CD55"]]
CD71 = Screens[["CD71"]]

# Get Guide Sequences
gfp.fa = "./PreProcessingMetaData/data/Cas13d_GFP_library.final.fa"
cd46.fa = "./PreProcessingMetaData/data/CD46_library.final.fa"
cd55.fa = "./PreProcessingMetaData/data/CD55_library.final.fa"
cd71.fa = "./PreProcessingMetaData/data/CD71_library.final.fa"
GUIDES.GFP  <- Biostrings::readDNAStringSet(filepath = gfp.fa, format = "fasta", use.names = T)
GUIDES.CD46  <- Biostrings::readDNAStringSet(filepath = cd46.fa, format = "fasta", use.names = T)
GUIDES.CD55  <- Biostrings::readDNAStringSet(filepath = cd55.fa, format = "fasta", use.names = T)
GUIDES.CD71  <- Biostrings::readDNAStringSet(filepath = cd71.fa, format = "fasta", use.names = T)




# Get nt freq
LetterFreq.gfp = GetLetterFreq(x = GFP, G = GUIDES.GFP, S=1, E=27)
LetterFreq.cd46 = GetLetterFreq(x = CD46, G = GUIDES.CD46, S=1, E=23)
LetterFreq.cd55 = GetLetterFreq(x = CD55, G = GUIDES.CD55, S=1, E=23)
LetterFreq.cd71 = GetLetterFreq(x = CD71, G = GUIDES.CD71, S=1, E=23)

g1 = LollipopPlot(FREQ = LetterFreq.gfp , NAME = "GFP: perfect match")
g2 = LollipopPlot(FREQ = LetterFreq.cd46 , NAME = "CD46: perfect match")
g3 = LollipopPlot(FREQ = LetterFreq.cd55 , NAME = "CD55: perfect match")
g4 = LollipopPlot(FREQ = LetterFreq.cd71 , NAME = "CD71: perfect match")

gg1 = GCcorPlot(FREQ = LetterFreq.gfp , NAME = "GFP: perfect match")
gg2 = GCcorPlot(FREQ = LetterFreq.cd46 , NAME = "CD46: perfect match")
gg3 = GCcorPlot(FREQ = LetterFreq.cd55 , NAME = "CD55: perfect match")
gg4 = GCcorPlot(FREQ = LetterFreq.cd71 , NAME = "CD71: perfect match")


# scale data
LetterFreq.gfp$ScaledCS = scale(LetterFreq.gfp$meanCS , center = T)[,1]
LetterFreq.cd46$ScaledCS = scale(LetterFreq.cd46$meanCS , center = T)[,1]
LetterFreq.cd55$ScaledCS = scale(LetterFreq.cd55$meanCS , center = T)[,1]
LetterFreq.cd71$ScaledCS = scale(LetterFreq.cd71$meanCS , center = T)[,1]
LetterFreq.comb = rbind.data.frame(LetterFreq.gfp, LetterFreq.cd46, LetterFreq.cd55, LetterFreq.cd71)


FREQ = LetterFreq.comb
c = cor(FREQ[,grep("MatchType|p|mean",colnames(FREQ), invert = T)],use = "complete.obs")
df = melt(c["ScaledCS",grep("ScaledCS", colnames(c) ,invert = T)])
rownames(df) <- gsub("\\.","|",rownames(df))
colnames(df) <- "pearson_coefficient"
df$Nucleotide <- rownames(df)    
df$type <- sapply(rownames(df), FUN = GetNTfreqType)
df$type <- factor( df$type, levels = c("nucleotide","nucleotides","di-nucleotide"))
df$Nucleotide <- gsub("T","U",df$Nucleotide)

G1 = ggdotchart(df, x = "Nucleotide", y = "pearson_coefficient",
                color = "type",                               # Color by groups
                palette = c( "#E7B800", "#FC4E07","#00AFBB"), # Custom color palette
                sorting = "descending",                       # Sort value in descending order
                add = "segments",                             # Add segments from y = 0 to dots
                add.params = list(color = "lightgray", size = 2), # Change segment color and size
                #group = "type",                               # Order by groups
                dot.size = 7,                                 # Large dot size
                label = df$Nucleotide,                        # Add mpg values as dot labels
                # label = round(df$pearson_coefficient,1),
                font.label = list(color = "white", size = 9, vjust = 0.5),        # Adjust label parameters
                ggtheme = theme_pubr(),                        # ggplot2 theme
                ylim=c(-0.25,0.25),
                title = "Combined: perfect match")



FREQ = LetterFreq.comb
cols = which(colnames(FREQ) == "pG|pC" | colnames(FREQ) == "ScaledCS")
dat = FREQ[,cols]
colnames(dat)[1] = "GC"

fit <- lm(  dat$ScaledCS ~ dat$GC )
r = summary(fit)$adj.r.squared
c = cor(dat , use = "complete.obs" )[2]

Y = sum(abs(range(dat$ScaledCS , na.rm = T)))
X = max(dat$GC , na.rm = T) - min(dat$GC , na.rm = T) 
R = X/Y

G2 = ggplot(dat, aes(x=GC, y=ScaledCS)) +
  geom_point(shape=20) +    # Use hollow circles
  geom_smooth(method=lm,   se=F, colour="#c82026")  +      
  geom_smooth(se = T, method = "loess", na.rm = T) +
  # geom_smooth(method = lm, formula = y ~ splines::bs(x, 3), se = T) +
  theme_classic() +
  ggtitle("Combined: perfect match") + 
  ylab("scaled log2(Top/Input)") + xlab("GC content") +
  annotate( geom = "text", label = paste0("rho = ",signif(c,2)), y = 4, x = 0.2, hjust = "inward") +
  annotate( geom = "text", label = paste0("R^2 = ",signif(r,2)), y = 3.5, x = 0.2, hjust = "inward") +
  annotate( geom = "text", label = paste0("p = ",signif(lmp(fit),2)), y = 3, x = 0.2, hjust = "inward") +  
  coord_fixed(ratio = R) 




lay <- rbind(c(1,1,2,2,2),
             c(3,3,4,4,4),
             c(5,5,6,6,6),
             c(7,7,8,8,8),
             c(9,9,10,10,10))
pdf("./figures/Note1_2Fig2bc.pdf", width = 10, height = 20, useDingbats = F)
grid.arrange(grobs = list( G2,G1,
                           gg1,g1,
                           gg2,g2,
                           gg3,g3,
                           gg4,g4) , layout_matrix = lay)
dev.off()



