#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(pheatmap, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(Rsamtools, quietly = T))
suppressMessages(library(GenomicAlignments, quietly = T))
suppressMessages(library(stringr, quietly = T))
suppressMessages(library(RColorBrewer, quietly = T))
suppressMessages(library(ggseqlogo, quietly = T))
suppressMessages(library(ggpubr, quietly = T))


###### sub-routines #################################


GetmmPos <- function(y){
  return( as.integer(strsplit(y, split = "_")[[1]][3] ))
}

GetDeltaCS <- function(x){
  

  # temporarily keep track of rownames
  x$rn <- x$GuideName
  
  x$DeltaCS <- NA
  
  #split by Guide
  GUIDE <- split(x, f = x$Guide)
  
  # for each guide return the delta CS
  for ( i in 1:length(GUIDE)){
    
    # skip non-targeting guides
    if(grepl("rc_0",names(GUIDE)[i]) == T){
      next
    }
    else if(grepl("rc_0",names(GUIDE)[i]) == F){
      #if only the parental guide but no permutation is present, assign 0 and next
      if ( nrow(GUIDE[[i]]) == 1 ){
        GUIDE[[i]]$DeltaCS <- 0
      }
      else if(nrow(GUIDE[[i]]) > 1){
        
        # determine reference guide
        ref <- which(GUIDE[[i]]$MatchType == "Perfect Match")
        if (length(ref) == 1){
          GUIDE[[i]]$DeltaCS = GUIDE[[i]]$meanCS - GUIDE[[i]]$meanCS[ref]
        }
        else{
          stop("Exiting. More than 1 reference guide. Please review code.")
        }
      } 
      else{
        stop("exiting. verify all guides have at least 1 element assigned")
      }
    }
    else{
      stop("exiting. verify guide names")
    }
  }
  
  # combine into its original format
  out <- do.call(rbind,GUIDE)
  
  # restore rownames
  rownames(out) <- NULL
  out <- out[,-which(colnames(x) == "rn")]
  
  #return output
  return(out)
} 



TranslatePval <- function(x){
  if( x > 0.05){
    return("")
  } else if (x >= 0.01 & x < 0.05 ){
    return("*")
  } else if (x >= 0.001 & x < 0.01 ){
    return("**")
  } else if ( x < 0.001 ){
    return("***")
  }
}


cor_mm_Pos <- function(x, PLOT=TRUE){
  
  #remove non-targeting
  x = x[grep("Non-Targeting",x$MatchType, invert = T),]
  #remove random double mismatches
  x = x[grep("Random Double",x$MatchType, invert = T),]
  
  # extract the mismatch position
  x$mmPos <- sapply( x$GuideName ,GetmmPos)
  
  # split by guide type
  TYPES <- split(x, f = x$MatchType)
  # isolate types all single mismatches
  FO <- TYPES[[grep("First Order",names(TYPES), invert = F)]]
  CD <- TYPES[[grep("Consecutive Double",names(TYPES), invert = F)]]
  CT <- TYPES[[grep("Consecutive Triple",names(TYPES), invert = F)]]
  # select corresponding perfect matching guides
  PM <- TYPES[[grep("Perfect Match",names(TYPES), invert = F)]]
  PM.FO <- PM[ which(PM$Guide %in% FO$Guide)  ,]
  PM.CD <- PM[ which(PM$Guide %in% CD$Guide)  ,]
  PM.CT <- PM[ which(PM$Guide %in% CT$Guide)  ,]

  # split by mismatch position
  FO.pos <- split(FO, f = FO$mmPos)
  CD.pos <- split(CD, f = CD$mmPos)
  CT.pos <- split(CT, f = CT$mmPos)
  
  # keep only CRISPR scores
  FO.pos.dCS = lapply(FO.pos, FUN=function(x){x[,"DeltaCS"]})
  CD.pos.dCS = lapply(CD.pos, FUN=function(x){x[,"DeltaCS"]})
  CT.pos.dCS = lapply(CT.pos, FUN=function(x){x[,"DeltaCS"]})
  FO.pos = lapply(FO.pos, FUN=function(x){x[,"meanCS"]})
  CD.pos = lapply(CD.pos, FUN=function(x){x[,"meanCS"]})
  CT.pos = lapply(CT.pos, FUN=function(x){x[,"meanCS"]})

  
  # for consecutive double and triple, expand the value to all mismatching nt. Currently the deltalog2foldchange is associated with only the first mismatching nt
  # consectutive double
  l = length(CD.pos)
  for (i in l:1){
    if ( i == length(CD.pos)){
      CD.pos[[i+1]] <- CD.pos[[i]]
      CD.pos.dCS[[i+1]] <- CD.pos.dCS[[i]]
      names(CD.pos)[i+1] = "27"
      names(CD.pos.dCS)[i+1] = "27"
    }else{
      CD.pos[[i+1]] <- c( CD.pos[[i]] , CD.pos[[i+1]] )
      CD.pos.dCS[[i+1]] <- c( CD.pos.dCS[[i]] , CD.pos.dCS[[i+1]] )
    }
  }
  # consectutive triple
  l = length(CT.pos)
  for (i in l:1){
    if ( i == length(CT.pos)){
      CT.pos[[i+2]] <- CT.pos[[i]]
      CT.pos[[i+1]] <- CT.pos[[i]]
      CT.pos.dCS[[i+2]] <- CT.pos.dCS[[i]]
      CT.pos.dCS[[i+1]] <- CT.pos.dCS[[i]]
      names(CT.pos)[i+1] = "26"
      names(CT.pos)[i+2] = "27"
      names(CT.pos.dCS)[i+1] = "26"
      names(CT.pos.dCS)[i+2] = "27"
    }else{
      CT.pos[[i+1]] <- c( CT.pos[[i]] , CT.pos[[i+1]] )
      CT.pos[[i+2]] <- c( CT.pos[[i]] , CT.pos[[i+2]] )
      CT.pos.dCS[[i+1]] <- c( CT.pos.dCS[[i]] , CT.pos.dCS[[i+1]] )
      CT.pos.dCS[[i+2]] <- c( CT.pos.dCS[[i]] , CT.pos.dCS[[i+2]] )
    }
  }
  
  # Get positional P value
  P.FO <- vector()
  P.CD <- vector()
  P.CT <- vector()
  
  for (j in 1:length(FO.pos)){   P.FO[j] <- signif(t.test(FO.pos[[j]] , PM.FO$meanCS, alternative = "less")$p.value, 2)   }
  p.FO.adj <- p.adjust(P.FO, method = "bonferroni", n = length(P.FO)) # correction for multiple testing
  P.FO.adj <- sapply(p.FO.adj, TranslatePval) # Translate Pval
  
  for (j in 1:length(CD.pos)){   P.CD[j] <- signif(t.test(CD.pos[[j]] , PM.CD$meanCS, alternative = "less")$p.value, 2)   }
  p.CD.adj <- p.adjust(P.CD, method = "bonferroni", n = length(P.CD)) # correction for multiple testing
  P.CD.adj <- sapply(p.CD.adj, TranslatePval) # Translate Pval
  
  for (j in 1:length(CT.pos)){   P.CT[j] <- signif(t.test(CT.pos[[j]] , PM.CT$meanCS, alternative = "less")$p.value, 2)   }
  p.CT.adj <- p.adjust(P.CT, method = "bonferroni", n = length(P.CT)) # correction for multiple testing
  P.CT.adj <- sapply(p.CT.adj, TranslatePval) # Translate Pval
  
  
  
  lbl <- as.data.frame(cbind( names(FO.pos)  ,P.FO.adj )) 
  colnames(lbl ) <- c("nt","value")
  lbl.df <- melt(lbl, id.vars = "nt")
  lbl.df$nt <- as.integer(as.character(lbl.df$nt))
  
  df <- melt(FO.pos.dCS)
  colnames(df)[2] = "mmPos"
  df$mmPos <- as.numeric(as.character(df$mmPos))
  #df <- melt(pos[,grep("mmPos|DeltaCS",colnames(pos))], id.vars = "mmPos")
  
  FO.means = (aggregate(value ~ mmPos, df, mean))
  FO.medians = (aggregate(value ~ mmPos, df, median))

  g1<-ggplot(df,aes(x=mmPos,y=value)) + 
    geom_boxplot(aes(group=mmPos),fill = "#f0f0f0",outlier.shape=20, outlier.size=0.01) + 
    # geom_jitter( shape = 4, size = 0.5, width = 0.2, colour = "darkgrey") +
    geom_smooth(method = 'loess' ,na.rm=T, span = 0.5) +
    ylim(c(-2,2)) +
    #xlim(c(28,0)) + 
    theme_classic() + ylab("delta log2foldchange") + xlab("guide position [nt]") +  ggtitle("single mismatches") + 
    theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = 3) +
    geom_hline(yintercept=0, linetype="solid", color = "black") + 
    theme(legend.position = "none") + 
    geom_text(data = lbl.df  ,mapping = aes(x = nt, y = 1.25, label = value, angle = 90), size=5, vjust = 0.9 ) + 
    scale_x_continuous(breaks = 28:0, labels=c("",as.character(c(27:1)),""))

  
  
  
  
  
  lbl <- as.data.frame(cbind( names(CD.pos)  ,P.CD.adj )) 
  colnames(lbl ) <- c("nt","value")
  lbl.df <- melt(lbl, id.vars = "nt")
  lbl.df$nt <- as.integer(as.character(lbl.df$nt))
  
  
  df <- melt(CD.pos.dCS)
  colnames(df)[2] = "mmPos"
  df$mmPos <- as.numeric(as.character(df$mmPos))
  
  CD.means = (aggregate(value ~ mmPos, df, mean))
  CD.medians = (aggregate(value ~ mmPos, df, median))
  
  g2<- ggplot(df,aes(x=mmPos,y=value)) + 
    geom_boxplot(aes(group=mmPos),fill = "#f0f0f0", outlier.shape=20, outlier.size=0.01) + 
    # geom_jitter( shape = 4, size = 0.5, width = 0.2, colour = "darkgrey") +
    geom_smooth(method = 'loess' ,na.rm=T, span = 0.5) +
    ylim(c(-2,2)) +
    #xlim(c(28,0)) + 
    theme_classic() + ylab("delta log2foldchange") + xlab("guide position [nt]") +  ggtitle("consecutive double mismatches") + 
    theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = 3) +
    geom_hline(yintercept=0, linetype="solid", color = "black") + 
    theme(legend.position = "none") + 
    geom_text(data = lbl.df  ,mapping = aes(x = nt, y = 1.25, label = value, angle = 90), size=5, vjust = 0.9 ) + 
    scale_x_continuous(breaks = 28:0, labels=c("",as.character(c(27:1)),""))
  

  
  
  
  lbl <- as.data.frame(cbind( names(CT.pos)  ,P.CT.adj )) 
  colnames(lbl ) <- c("nt","value")
  lbl.df <- melt(lbl, id.vars = "nt")
  lbl.df$nt <- as.integer(as.character(lbl.df$nt))
  
  
  df <- melt(CT.pos.dCS)
  colnames(df)[2] = "mmPos"
  df$mmPos <- as.numeric(as.character(df$mmPos))
  
  CT.means = (aggregate(value ~ mmPos, df, mean))
  CT.medians = (aggregate(value ~ mmPos, df, median))
  
  g3<- ggplot(df,aes(x=mmPos,y=value)) + 
    geom_boxplot(aes(group=mmPos),fill = "#f0f0f0", outlier.shape=20, outlier.size=0.01) + 
    # geom_jitter( shape = 4, size = 0.5, width = 0.2, colour = "darkgrey") +
    geom_smooth(method = 'loess' ,na.rm=T, span = 0.25) +
    ylim(c(-2,2)) + 
    #xlim(c(28,0)) + 
    theme_classic() + ylab("delta log2foldchange") + xlab("guide position [nt]") +  ggtitle("consecutive triple mismatches") + 
    theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = 3) +
    geom_hline(yintercept=0, linetype="solid", color = "black") + 
    theme(legend.position = "none") + 
    geom_text(data = lbl.df  ,mapping = aes(x = nt, y = 1.25, label = value, angle = 90), size=5, vjust = 0.9 ) + 
    scale_x_continuous(breaks = 28:0, labels=c("",as.character(c(27:1)),""))
  
  ma = cbind(FO.means,CD.means[,2],CT.means[,2],FO.medians[,2],CD.medians[,2],CT.medians[,2])
  colnames(ma) = c("mmPos","FO.mean","CD.mean","CT.mean","FO.median","CD.median","CT.median")
  
  return(list(g1,g2,g3,ma))
}



DeconvolveSingleMismatches <- function(SM){
  #SM = sm
  
  #Get MD-tag column and Sequence
  col.MD <- which(colnames(mcols(SM)) == "MD")
  col.SEQ <- which(colnames(mcols(SM)) == "seq")
  
  #convert to GRanges
  SM <- as(SM, "GRanges")
  
  # for the shifting, the metacolumns need to be removed
  SM.nm <- SM
  mcols(SM.nm) <- NULL
  
  # vector with shift position (from MD tag)
  first <- as.integer(sapply(strsplit(mcols(SM)[,col.MD],split = "[A-Z]"),"[[",1))
  SM.new <- shift(resize(SM.nm, width=1, fix="end"),shift = first)
  
  # retrieve the identity of the mismatched base
  reference.base <- unlist(lapply(str_extract(mcols(SM)[,col.MD], "[A-Z]"), FUN=complement.base))
  
  mutated.seq <- as.vector(sapply(X=as.vector(subseq(mcols(SM)[,col.SEQ], start=first+1, width=1)),FUN=complement.base))  # reverse
  
  mcols(SM.new) <- mcols(SM) 
  mcols(SM.new)[ncol(mcols(SM.new))+1] <- mutated.seq
  mcols(SM.new)[ncol(mcols(SM.new))+1] <- reference.base
  mcols(SM.new)[ncol(mcols(SM.new))+1] <- paste0(reference.base,"-",mutated.seq)
  mcols(SM.new)[ncol(mcols(SM.new))+1] <- 27-first
  colnames( mcols(SM.new) )[c(11:14)] <- c("mutated.seq", "reference.base", "conversion", "guidePos" )
  
  return(SM.new)
  
}


complement.base <- function(base){ 
  if(base == 'A' | base ==  'a') base<- "T" 
  else if(base == 'T' | base == 't') base<- "A"
  else if(base == 'G' | base == 'g') base<- "C"
  else if(base == 'C' | base == 'c') base<- "G"
  return(base)}

Get_MeanDeltaCSbyPos = function(z){
  byPos <- split(z , f = mcols(z)$guidePos)
  MeanDeltaCSbyPos <- sapply(byPos , FUN = function(j){ mean( mcols(j)$DeltaCS, na.rm = T) })
  return(MeanDeltaCSbyPos)
}

Get_MeanCSbyPos = function(z){
  byPos <- split(z , f = mcols(z)$guidePos)
  MeanCSbyPos <- sapply(byPos , FUN = function(j){ mean( mcols(j)$meanCS, na.rm = T) })
  return(MeanCSbyPos)
}

DeconvolveDoubleMismatches <- function(DM){
  #DM = dm
  
  #Get MD-tag column and Sequence
  col.MD <- which(colnames(mcols(DM)) == "MD")
  col.SEQ <- which(colnames(mcols(DM)) == "seq")
  
  #convert to GRanges
  DM <- as(DM, "GRanges")
  
  # for the shifting, the metacolumns need to be removed
  DM.nm <- DM
  mcols(DM.nm) <- NULL
  
  # vector with shift position (from MD tag)
  first <- as.integer(sapply(strsplit(mcols(DM)[,col.MD],split = "[A-Z]"),"[[",1))
  second.offset <- as.integer(sapply(strsplit(mcols(DM)[,col.MD],split = "[A-Z]"),"[[",2))
  second = first+second.offset+1
  DM.new <- shift(resize(DM.nm, width=1, fix="end"),shift = second)
  
  # retrieve the identity of the mismatched base
  ref.first.base <- as.character(sapply(sapply(strsplit(mcols(DM)[,col.MD],split = "[0-9]+"),"[[",2),FUN=complement.base))
  ref.second.base <- as.character(sapply(sapply(strsplit(mcols(DM)[,col.MD],split = "[0-9]+"),"[[",3),FUN=complement.base))
  
  mut.first.seq <- as.vector(sapply(X=as.vector(subseq(mcols(DM)[,col.SEQ], start=first+1, width=1)),FUN=complement.base))  # reverse
  mut.second.seq <- as.vector(sapply(X=as.vector(subseq(mcols(DM)[,col.SEQ], start=second+1, width=1)),FUN=complement.base))  # reverse
  
  mcols(DM.new) <- mcols(DM) 
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- mut.first.seq
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- mut.second.seq
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- ref.first.base
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- ref.second.base
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- paste0(ref.first.base,"-",mut.first.seq)
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- paste0(ref.second.base,"-",mut.second.seq)
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- 27-second
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- 27-first
  mcols(DM.new)[ncol(mcols(DM.new))+1] <- second.offset+1
  colnames( mcols(DM.new) )[c(11:19)] <- c("mut.sec", "mut.first", "ref.sec","ref.first","conv.sec","conv.first", "pos.first","pos.second","distance" )
  
  return(DM.new)
  
}

Get_deltaCS_by_distance = function(x){
  D <- split(x , f = mcols(x)$distance) # seperate out by distance to first conversion position
  deltaCS_by_distance = sapply(D, FUN = function(y){mean(mcols(y)$DeltaCS , na.rm = T)})
  return(deltaCS_by_distance)
}

cor_mm_PosRD = function(x, REF = Results){
  
  # get reference genes
  refs = unique(sapply(mcols(x)$qname, FUN=function(z){ strsplit( z , split = "_")[[1]][1]  }))
  PM.CS = REF[ which(REF$GuideName %in% refs) , "meanCS"]
  
  
  # split by first mismatch position
  FirstPos <- split(x , f = mcols(x)$pos.first)
  # split by second mismatch position
  SecondPos <- split(x , f = mcols(x)$pos.second)
  
  # retrieve only the deltaCS values
  FirstPos.CS = lapply(FirstPos, FUN = function(y){mcols(y)[,"meanCS"]})
  SecondPos.CS = lapply(SecondPos, FUN = function(y){mcols(y)[,"meanCS"]})
  FirstPos.deltaCS = lapply(FirstPos, FUN = function(y){mcols(y)[,"DeltaCS"]})
  SecondPos.deltaCS = lapply(SecondPos, FUN = function(y){mcols(y)[,"DeltaCS"]})
  
  # combine delta CS values for each position.
  # each delta CS gets counted twice; once for each mismatch position 
  RD.combined = list()
  RD.combined.CS = list()
  pos = as.character(1:27)
  for ( i in 1:length(pos)){
    RD.combined[[i]]  =  c(FirstPos.deltaCS[[pos[i]]],SecondPos.deltaCS[[pos[i]]])
    RD.combined.CS[[i]]  =  c(FirstPos.CS[[pos[i]]],SecondPos.CS[[pos[i]]])
  }
  names(RD.combined) = pos
  names(RD.combined.CS) = pos
  
  P.RD = vector()
  for (j in 1:length(RD.combined.CS)){   P.RD[j] <- signif(t.test(RD.combined.CS[[j]] , PM.CS, alternative = "less")$p.value, 2)   }
  p.RD.adj <- p.adjust(P.RD, method = "bonferroni", n = length(P.RD)) # correction for multiple testing
  P.RD.adj <- sapply(p.RD.adj, TranslatePval) # Translate Pval
  
  
  lbl <- as.data.frame(cbind( names(RD.combined.CS)  ,P.RD.adj )) 
  colnames(lbl ) <- c("nt","value")
  lbl.df <- melt(lbl, id.vars = "nt")
  lbl.df$nt <- as.integer(as.character(lbl.df$nt))
  
  
  df <- melt(RD.combined)
  colnames(df)[2] = "mmPos"
  df$mmPos <- as.numeric(as.character(df$mmPos))
  
  RD.means = (aggregate(value ~ mmPos, df, mean))
  RD.medians = (aggregate(value ~ mmPos, df, median))
  
  
  
  g4 = ggplot(df,aes(x=mmPos,y=value)) + 
    geom_boxplot(aes(group=mmPos),fill = "#f0f0f0", outlier.shape=20, outlier.size=0.01) + 
    # geom_jitter( shape = 4, size = 0.5, width = 0.2, colour = "darkgrey") +
    geom_smooth(method = 'loess' ,na.rm=T, span = 0.25) +
    ylim(c(-2,2)) + 
    #xlim(c(28,0)) + 
    theme_classic() + ylab("delta log2foldchange") + xlab("guide position [nt]") +  ggtitle("random double mismatches") + 
    theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = 3) +
    geom_hline(yintercept=0, linetype="solid", color = "black") + 
    theme(legend.position = "none") + 
    geom_text(data = lbl.df  ,mapping = aes(x = nt, y = 1.25, label = value, angle = 90), size=5, vjust = 0.9 ) + 
    scale_x_continuous(breaks = 28:0, labels=c("",as.character(c(27:1)),""))
  
  
  ma = cbind(RD.means,RD.medians[,2])
  colnames(ma) = c("mmPos","RD.mean","RD.median")
  
  return(list(g4,ma))
}


########################## sub-routines end  ########

###### execute ######################################


#load data
# Read combined result table
Results = read.delim('./data/CombinedTilingScreenResults.csv' , sep = ',', header = T, stringsAsFactors = F)


# Restrict to single Screens
Screens = split(Results, f = Results$Screen)
Results = Screens[["GFP"]]



##################  Get deltaCS
Results <-  GetDeltaCS(Results)



##################  Analyze straight positional effects
# Transform data and plot
output = cor_mm_Pos(Results)

#pdf("./figures/Fig1f.pdf", width = 5, height = 3, useDingbats = F)
# grid.arrange(arrangeGrob(grobs= output[1],ncol=1))
#dev.off()





##################  Analyze positional effects by base change

#Load reference bam alignment file to get conversion info
BAM = "./data/GFP_library.Inputmatch.bam"
#metacolumn parameters for bam file loading
p1 <- ScanBamParam(what=c("qname","flag","rname","strand","pos","mapq","cigar","seq","qual"),tag=c('MD'), reverseComplement = FALSE)
bam <- readGAlignments(BAM, param=p1)
sm <- bam[grep("_FirstOrder_",mcols(bam)$qname)]
colnames(mcols(sm))[grep("strand", colnames(mcols(sm)))] = "str"
SM <- DeconvolveSingleMismatches(sm)

# # how many (single) coversions are there
# pdf("./figures/SingleNtConversionCount.pdf", width = 4, height = 4, useDingbats = F)
# par(pty="s")
# t=table(mcols(SM)$conversion)
# names(t) = gsub("T","U",names(t))
# barplot(t, las=2, ylab="frequency")
# dev.off()

# # how are the conversions represented/distributed
# CONV <- split(SM, f = mcols(SM)$conversion) # seperate out by conversion type
# names(CONV) = gsub("T","U",names(CONV))
# ConvByPos <- lapply(CONV, FUN=function(x){table(mcols(x)$guidePos)}) # return positions of respective conversions
# ma <- matrix(0, ncol=27, nrow=length(ConvByPos))
# colnames(ma) <- seq(1,27,1)
# rownames(ma) <- names(ConvByPos)
# for (i in 1:length(ConvByPos)){
#   row <- which(rownames(ma) == names(ConvByPos)[i])
#   for (j in 1:length(ConvByPos[[i]])){
#     col <- which(colnames(ma) == names(ConvByPos[[i]])[j])
#     ma[row,col] <- ConvByPos[[i]][j]
#   }
# }
# pheatmap(ma, cluster_rows = F, cluster_cols = F, cellwidth = 12, cellheight = 12, gaps_row = c(3,6,9), main = "Conversion distribution",filename = "./figures/SingleNtConversionDistribution.pdf")


# Isolate single nt mismatches
FO = Results[Results$MatchType == "First Order",]
idx <- match( mcols(SM)$qname , FO$GuideName ) #match by name
mcols(SM)$DeltaCS <- FO$DeltaCS[idx] # add deltaCS
mcols(SM)$meanCS <- FO$meanCS[idx] # add meanCS
CONV <- split(SM, f = mcols(SM)$conversion) # seperate out by conversion type
names(CONV) = gsub("T","U",names(CONV))

# # Do different conversions have higher/lower deltaCS irrespective of their relative position?
# DeltaCSbyConv <- lapply(CONV, FUN=function(x){mcols(x)$DeltaCS}) # return deltaCS of respective conversions
# df <- melt(DeltaCSbyConv)
# df$RefBase <- as.character(sapply(df$L1, FUN = function(x){strsplit(x, split = "-")[[1]][1]}))
# 
# g1 = ggplot(df, aes(fill = RefBase, x=L1 , y=value)) +
#   geom_boxplot(outlier.shape=20, outlier.size=0.5) +
#   theme_classic() + ylab("delta log2foldchange") + xlab("base mismatch") + ylim(c(-2,2)) + ggtitle("single mismatches") +
#   theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = 3) +
#   scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) +
#   geom_hline(yintercept=0, linetype="solid", color = "black")
# 
# 
# MeanCSbyConv <- lapply(CONV, FUN=function(x){mcols(x)$meanCS}) # return deltaCS of respective conversions
# df <- melt(MeanCSbyConv)
# df$RefBase <- as.character(sapply(df$L1, FUN = function(x){strsplit(x, split = "-")[[1]][1]}))
# 
# g2 = ggplot(df, aes(fill = RefBase, x=L1 , y=value)) +
#   geom_boxplot(outlier.shape=20, outlier.size=0.5) +
#   theme_classic() + ylab("log2(Bin1/Input)") + xlab("base mismatch") + ylim(c(-2,2)) + ggtitle("single mismatches") +
#   theme(axis.text.x = element_text(angle = 90, hjust = 0, vjust = 0.5)) + coord_fixed(ratio = 3) +
#   scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) +
#   geom_hline(yintercept=0, linetype="solid", color = "black")
# 
# 
# pdf('./figures/SingleNtConversions.pdf', width = 8, height = 3, useDingbats = F)
# grid.arrange(arrangeGrob(grobs= list(g2,g1),nrow=1))
# dev.off()



# Get the mean deltaCS by conversion and by position
MeanDeltaCSbyPos <- lapply( CONV , Get_MeanDeltaCSbyPos )

# transform into a positional matrix
ma <- matrix(NA, ncol=27, nrow=length(MeanDeltaCSbyPos))
colnames(ma) <- seq(1,27,1)
rownames(ma) <- names(MeanDeltaCSbyPos)
for (i in 1:length(MeanDeltaCSbyPos)){
  row <- which(rownames(ma) == names(MeanDeltaCSbyPos)[i])
  for (j in 1:length(MeanDeltaCSbyPos[[i]])){
    col <- which(colnames(ma) == names(MeanDeltaCSbyPos[[i]])[j])
    ma[row,col] <- MeanDeltaCSbyPos[[i]][j]
  }
}

A = colMeans(ma[grep("A-",rownames(ma)),], na.rm = T)
C = colMeans(ma[grep("C-",rownames(ma)),], na.rm = T)
G = colMeans(ma[grep("G-",rownames(ma)),], na.rm = T)
U = colMeans(ma[grep("U-",rownames(ma)),], na.rm = T)

ma <- rbind(ma,A,C,G,U)

# define symmetric breaks
#mn = ma[which(ma == min(ma, na.rm = T))] 
#mx = ma[which(ma == max(ma, na.rm = T))] 
#s <- sum(range(abs(-(max(abs(mn),abs(mx))) ), abs(max(abs(mn),abs(mx)))))
#breaksList = seq(-(max(abs(mn),abs(mx))), abs(max(abs(mn),abs(mx))), by = s/100)

# set a limit to the colorscale
breaksList = seq(-0.75, 0.75, by = 1/100)

# plot
ma = ma[,ncol(ma):1]

deltaCS.FO.byBase = ma

# pheatmap(ma, cluster_rows = F, cluster_cols = F, cellwidth = 12, cellheight = 12, gaps_row = c(3,6,9,12,12,12), main = "deltaCS by position by mismatch",
#          color = colorRampPalette((brewer.pal(n = 9, name = "RdGy")))(length(breaksList)), breaks = breaksList, na_col = "black",
#          filename = "./figures/FigS9b.pdf")







# Get the meanCS by conversion and by position
MeanCSbyPos <- lapply( CONV , Get_MeanCSbyPos )

# transform into a positional matrix
ma <- matrix(NA, ncol=27, nrow=length(MeanCSbyPos))
colnames(ma) <- seq(1,27,1)
rownames(ma) <- names(MeanCSbyPos)
for (i in 1:length(MeanCSbyPos)){
  row <- which(rownames(ma) == names(MeanCSbyPos)[i])
  for (j in 1:length(MeanCSbyPos[[i]])){
    col <- which(colnames(ma) == names(MeanCSbyPos[[i]])[j])
    ma[row,col] <- MeanCSbyPos[[i]][j]
  }
}


A = colMeans(ma[grep("A-",rownames(ma)),], na.rm = T)
C = colMeans(ma[grep("C-",rownames(ma)),], na.rm = T)
G = colMeans(ma[grep("G-",rownames(ma)),], na.rm = T)
U = colMeans(ma[grep("U-",rownames(ma)),], na.rm = T)

ma <- rbind(ma,A,C,G,U)


# # define symmetric breaks
# mn = ma[which(ma == min(ma, na.rm = T))]
# mx = ma[which(ma == max(ma, na.rm = T))]
# s <- sum(range(abs(-(max(abs(mn),abs(mx))) ), abs(max(abs(mn),abs(mx)))))
# breaksList = seq(-(max(abs(mn),abs(mx))), abs(max(abs(mn),abs(mx))), by = s/100)

# set a limit to the colorscale
breaksList = seq(-0.75, 0.75, by = 1/100)

# plot
ma = ma[,ncol(ma):1]
# pheatmap(ma, cluster_rows = F, cluster_cols = F, cellwidth = 12, cellheight = 12, gaps_row = c(3,6,9,12,12,12), main = "meanCS by position by mismatch",
#          color = colorRampPalette(rev(brewer.pal(n = 9, name = "RdGy")))(length(breaksList)), breaks = breaksList,na_col = "black",
#          filename = "./figures/FigS9c.pdf")



# # check for the sequence nt distribution for all 100 guides used here
# pm <- bam[grep( "rc_|First|Double|Triple|Random", mcols(bam)$qname, invert=T)]
# names <- unique(as.character(sapply(mcols(SM)$qname , FUN = function(z){ strsplit(z, split = "_")[[1]][1]})))
# pm <- pm[which( mcols(pm)$qname %in% names)]
# seqs <- reverseComplement(mcols(pm)$seq)
# pdf("./figures/SingleNtConversion_ReferenceGuidesSequenceProbabilities.pdf", width = 6, height = 2, useDingbats = F)
# ggplot() + geom_logo( as.character(seqs), seq_type = "DNA",  method = "probability" ) + xlab("guide position [nt]") +
#   ggtitle("sequence probability of single nt mismatch reference guides (n=100)") + theme_logo() +
#   theme(plot.title = element_text(size = 10))
# dev.off()




##################  Analyze positional effects by base change for random double mismatches



#Load reference bam alignment file to get conversion info
BAM = "./data/GFP_library.Inputmatch.bam"
#metacolumn parameters for bam file loading
p1 <- ScanBamParam(what=c("qname","flag","rname","strand","pos","mapq","cigar","seq","qual"),tag=c('MD'), reverseComplement = FALSE)
bam <- readGAlignments(BAM, param=p1)
dm <- bam[grep("_randomDouble_",mcols(bam)$qname)]
colnames(mcols(dm))[grep("strand", colnames(mcols(dm)))] = "str"
DM <- DeconvolveDoubleMismatches(dm)



# Isolate random double nt mismatches
RD = Results[Results$MatchType == "Random Double",]
idx <- match( mcols(DM)$qname , RD$GuideName ) #match by name
mcols(DM)$DeltaCS <- RD$DeltaCS[idx] # add deltaCS
mcols(DM)$meanCS <- RD$meanCS[idx] # add meanCS
FirstPos <- split(DM, f = mcols(DM)$pos.first) # seperate out by first conversion position (closets to guide start)

deltaCS_by_distance <- lapply(FirstPos, Get_deltaCS_by_distance)


ma <- matrix(NA, ncol = 26, nrow=26)
colnames(ma) <- seq(1,26,1)
rownames(ma) <- seq(1,26,1)
for ( i in 1:length(deltaCS_by_distance)){
  col = as.integer(names(deltaCS_by_distance)[i])
  for(k in 1:length(deltaCS_by_distance[[i]])){
    row = 27-as.integer(names(deltaCS_by_distance[[i]])[k])
    ma[row,col] <- deltaCS_by_distance[[i]][k]
  }
}

#reverse column order
ma = ma[,ncol(ma):1]
DeltaCSbyDist = ma

# # define symmetric breaks
# mn = ma[which(ma == min(ma, na.rm = T))] 
# mx = ma[which(ma == max(ma, na.rm = T))] 
# s <- sum(range(abs(-(max(abs(mn),abs(mx))) ), abs(max(abs(mn),abs(mx)))))
# breaksList = seq(-(max(abs(mn),abs(mx))), abs(max(abs(mn),abs(mx))), by = s/100)


# set a limit to the colorscale
breaksList = seq(-0.75, 0.75, by = 1/100)

# pheatmap(ma, cluster_rows = F, cluster_cols = F,  cellwidth = 12, cellheight = 12, main = "deltaCS by position by distance",   
#          color = colorRampPalette((brewer.pal(n = 9, name = "RdGy")))(length(breaksList)), 
#          breaks = breaksList, labels_row = seq(26,1,-1),na_col = "black", border_color = NA,  
#          filename = "./figures/FigS9d.pdf")



L = list()
seedpos = seq(15,21,1)
DM.seed = DM[which(mcols(DM)$pos.first %in% seedpos | mcols(DM)$pos.second %in% seedpos)]
DM.bothseed = DM[which(mcols(DM)$pos.first %in% seedpos & mcols(DM)$pos.second %in% seedpos)]
DM.seed = DM.seed[-which(mcols(DM.seed)$qname %in% mcols(DM.bothseed)$qname )]
DM.noseed = DM[which(!(mcols(DM)$pos.first %in% seedpos) & !(mcols(DM)$pos.second %in% seedpos))]
L[[1]] = DM.bothseed
L[[2]] = DM.seed
L[[3]] = DM.noseed
names(L) = c("2", "1","0")

df = cbind.data.frame( c(rep(names(L)[1],length(L[[1]]$DeltaCS)),rep(names(L)[2],length(L[[2]]$DeltaCS)),rep(names(L)[3],length(L[[3]]$DeltaCS))),
       c(L[[1]]$DeltaCS, L[[2]]$DeltaCS, L[[3]]$DeltaCS) )
colnames(df) = c("variable","value")
df$variable = factor( df$variable , levels = rev(c("2", "1","0")))

pdf( "./figures/FigS4b_inset.pdf", width = 3, height = 3, useDingbats = F)
ggplot(df , aes( x = variable , y = value, fill = variable)) + 
  geom_violin(trim = T, scale = "area") +
  geom_boxplot(outlier.size = -1, width = 0.1, fill = "white") +
  theme_classic() + xlab("Mismatches in seed") + ylab("log2FC")+
  scale_fill_manual(values=rev(c( "#af1e23","#dc6950", "#facdb5")))+
  theme(legend.position = "none") +
  coord_fixed(0.6)
dev.off()

pdf( "./figures/FigS4b_inset2.pdf", width = 3, height = 3, useDingbats = F)
my_comparisons <- list( c("1", "2"), c("0", "1"), c("0", "2") )
ggboxplot(df, x = "variable", y = "value",ylab = "log2FC",xlab = "",
          color = "variable", palette = rev(c( "#af1e23","#dc6950", "#facdb5")), add = "jitter", outlier.shape = NA, add.params = list(shape = 8, size = 0.1)) + 
  stat_compare_means(comparisons = my_comparisons, method = "wilcox.test") # Add pairwise comparisons p-value
dev.off()




######## Get delta CS value summarized across both mismatch positions
output2 = cor_mm_PosRD( x=DM , REF = Results)






# plot
pdf('./figures/Fig1f_FigS4_Mismatches.pdf', width = 5, height = 12, useDingbats = F)
grid.arrange(arrangeGrob(grobs= c(output[1:2],output2[1],output[3]),ncol=1))
dev.off()






######## Get combined heatmap

# output delta CS value summarized across mismatch positions for single/consecutive double/consecutive triple
# transform output
tmp = t(output[[4]])[2:4,]
tmp = tmp[,ncol(tmp):1]

# output delta CS value summarized across mismatch positions for random double
# transform output
tmp2 = t(output2[[2]])[2:3,]
tmp2 = tmp2[,ncol(tmp2):1]

# combine with single mismatches by bases
comb.ma = rbind( cbind(NA , DeltaCSbyDist), tmp2[1,], deltaCS.FO.byBase , tmp[1:3,] )


colnames(comb.ma) = 27:1
rownames(comb.ma) = c(rev(c("1","2","3","4","5","6","7","8","9","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26")),
                      "random double mismatch" ,
                      "A-C","A-G","A-U","C-A","C-G","C-U","G-A","G-C","G-U","U-A","U-C","U-G",
                      "A","C","G","U",
                      "single mismatch" ,"consecutive double mismatch",  "consecutive triple mismatch")


# set a limit to the colorscale
breaksList = seq(-0.75, 0.75, by = 1/100)

pheatmap(comb.ma, cluster_rows = F, cluster_cols = F,  cellwidth = 12, cellheight = 12, show_rownames = T,
         gaps_row = c(26,27,27,27, 30,33,36,39,43,43,43 ),
         color = colorRampPalette((brewer.pal(n = 9, name = "RdGy")))(length(breaksList)), 
         breaks = breaksList,na_col = "black", border_color = NA,
         filename = "./figures/Fig1g_FigS4b.pdf")




