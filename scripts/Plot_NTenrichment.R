#!/usr/bin/env Rscript

# Excerpt from Doench et al 2014 Nature Biotechnology:
# "Specifically, we looked for statistical enrichment or depletion of sgRNAs with a given sequence feature among the 20% most active sgRNAs for the same gene target, as these high-activity sgRNAs are of most interest (Fig. 3a and Supplementary Table 8). Within the sgRNA sequence, the most significant differences appeared at position 20, the nucleotide immediately adjacent to the PAM"
# "Figure legend: P-values of observing the conditional probability of a guide with a percent-rank activity of >0.8 under the null distribution examined at every position including the 4 nt upstream of the sgRNA target site, the 20 nt of sgRNA complementarity, the PAM and the 3 nt downstream of the sgRNA target sequence. P-values were calculated from the binomial distribution with a baseline probability of 0.2 using 1,841 CDS-targeting guides."

# Here we made the following changes in our implementation:
# 1) For the binomial testing we used the respective nucleotide frequency within the target as opposed to "baseline probability of 0.2". A baseline probability of 0.2 naturally overestimates the importance of G an C nucleotides within coding regions in human due to uneven nucleotide representation and makes it hard for U and A to be found significantly enriched.
# 2) We applied correction for multiple testing, which made most observed changes insignificant




###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(Biostrings, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(gridExtra, quietly = T))


###### sub-routines #################################

GetMatch = function(j,y = FASTA, VAL = "pos", MM=1 , TARGET = "GFP"){
  if (TARGET == "GFP"){
    res = vmatchPattern( pattern = reverseComplement(DNAString(j)) , subject = y , max.mismatch = MM)$pHKO32_GFP
  }
  else if(TARGET == "CD46"){
    res = vmatchPattern( pattern = reverseComplement(DNAString(j)) , subject = y , max.mismatch = MM)$ENST00000367042.1
  }
  else if(TARGET == "CD55"){
    res = vmatchPattern( pattern = reverseComplement(DNAString(j)) , subject = y , max.mismatch = MM)$ENST00000367064.3
  }
  else if(TARGET == "CD71"){
    res = vmatchPattern( pattern = reverseComplement(DNAString(j)) , subject = y , max.mismatch = MM)$ENST00000360110.4
  }else{
    stop("exiting. please refine the target gene. Use \"GFP\",\"CD46\",\"CD55\" or \"CD71\".")
  }
  
  if (length(res) == 0){
    return("Error. No match found")
  }
  else if (length(res) == 1){
    if (VAL == "pos"){
      return(end(res))
    }
    else if (VAL == "TargetSeq"){
      str=substring(y,start(res)-4,end(res)+4)
      
      if (nchar(str) < 4){
        return(NA)
      }
      else{
        return(str)
      }
    }
    else if (VAL == "upstream"){
      str=substring(y,end(res)+1,end(res)+4)
      
      if (nchar(str) < 4){
        return(NA)
      }
      else{
        return(str)
      }
      
    }
    
    else if (VAL == "downstream"){
      str=substring(y,start(res)-4,start(res)-1)
      
      if (nchar(str) < 4){
        return(NA)
      }
      else{
        return(str)
      }
    }
  }
  else{
    return(print(c("Error. More than 1 match found for ", j)))
  }
}


Get_p_binomal = function(TOP = top20[,1:35], BACKGROUND = all[,1:35], LowerTail = TRUE, MultipleTestingCor = T, EffectSize = FALSE, MEASURE = "foldchange"){
  
  out <- matrix(1,ncol = ncol(TOP), nrow = 4)
  rownames(out) = c("A","C","G","U")
  colnames(out) = colnames(TOP)
  
  if (EffectSize == TRUE){
    
    if (MEASURE == "foldchange"){
      
      for ( i in 1:ncol(BACKGROUND)){
        
        # this calculates a fold change in probability (top / background)
        # This is not very well suited given varying backgrounds 
        # (e.g.  0.7/0.6=1.16 and 0.3/0.2=1.5 have the same delta probability change (0.1) but very different fold changes)
        
        # Get Background nucleotide probabilities from all guides, per position
        NT.probs <- table(as.character(BACKGROUND[,i]))/sum(table(as.character(BACKGROUND[,i])))
        
        # Get Top20 counts 
        Top.probs <- table(as.character(TOP[,i]))/sum(table(as.character(TOP[,i])))
        
        # build fold change (=effect size)
        out[,i] = Top.probs/NT.probs
      }
      
    }
    else {
      
      # this calculates a delta probability change (top - background)
      
      for ( i in 1:ncol(BACKGROUND)){
        
        # Get Background nucleotide probabilities from all guides, per position
        NT.probs <- table(as.character(BACKGROUND[,i]))/sum(table(as.character(BACKGROUND[,i])))
        
        # Get Top20 counts 
        Top.probs <- table(as.character(TOP[,i]))/sum(table(as.character(TOP[,i])))
        
        # build fold change (=effect size)
        out[,i] = Top.probs - NT.probs
      }
      
    }
    
    
    
  }

  
  else{
    for ( i in 1:ncol(BACKGROUND)){
      
      # Get Background nucleotide probabilities from all guides, per position
      NT.probs <- table(as.character(BACKGROUND[,i]))/sum(table(as.character(BACKGROUND[,i])))
      
      # Get Top20 counts 
      Top.Cnts <- table(TOP[,i])
      Top.probs <- table(as.character(TOP[,i]))/sum(table(as.character(TOP[,i])))
      
      
      p.A = pbinom(Top.Cnts["A"], size=sum(Top.Cnts), prob = NT.probs["A"], lower.tail = LowerTail, log.p = F)
      p.C = pbinom(Top.Cnts["C"], size=sum(Top.Cnts), prob = NT.probs["C"], lower.tail = LowerTail, log.p = F)
      p.G = pbinom(Top.Cnts["G"], size=sum(Top.Cnts), prob = NT.probs["G"], lower.tail = LowerTail, log.p = F)
      p.U = pbinom(Top.Cnts["T"], size=sum(Top.Cnts), prob = NT.probs["T"], lower.tail = LowerTail, log.p = F)
      
      # Correction for multiple testing by Bonferroni
      if (MultipleTestingCor){
        out["A",i] <- p.adjust( p.A, method = "bonferroni", n = (length(NT.probs)*ncol(TOP)))
        out["C",i] <- p.adjust( p.C, method = "bonferroni", n = (length(NT.probs)*ncol(TOP)))
        out["G",i] <- p.adjust( p.G, method = "bonferroni", n = (length(NT.probs)*ncol(TOP)))
        out["U",i] <- p.adjust( p.U, method = "bonferroni", n = (length(NT.probs)*ncol(TOP)))
      }
      else{
        out["A",i] <-  p.A
        out["C",i] <-  p.C
        out["G",i] <-  p.G
        out["U",i] <-  p.U
      }
      #gbinom(sum(Top.Cnts), NT.probs["C"],  scale = T)
      #abline(v =Top.Cnts["C"])
    }
    
  }
  return(out)
}




Assess_nt_preference <- function(x = Results, G = GUIDES, FASTA=FA, TYPE = "Perfect Match",  PERSPECTIVE = "target", guideLen = 27, DATA = FALSE){
  
  if( !TYPE %in% c(  "Perfect Match" , "First Order")){
    stop("Exiting. TYPE needs to be \"Perfect Match\" or \"First Order\"")
  }
  
  if( grepl( "GFP" , names(FASTA)) ){
    target="GFP"
  }else if( grepl( "ENST00000367042" , names(FASTA)) ){
    target="CD46"
  }else if( grepl( "ENST00000367064" , names(FASTA)) ){
    target="CD55"
  }else if( grepl( "ENST00000360110" , names(FASTA)) ){
    target="CD71"
  }
  else{
    target="GFP"
  }
  
  
  
  x <- x[grep("rc_", x$GuideName, invert = T),] #remove non-targeting
  x <- x[grep("intron", x$GuideName, invert = T),] #remove intronic
  x <- x[grep("RevCom", x$GuideName, invert = T),] #remove reverse complement negative controls
  x <- x[grep("Len", x$GuideName, invert = T),] #remove Length Variants
  x <- x[grep("randomDouble", x$GuideName, invert = T),] #remove random double mismatches
  x <- x[grep("consec", x$GuideName, invert = T),] #remove random double mismatches
  
  
  
  
  idx = match( x$GuideName , names(G))
  
  if (all( x$GuideName == names(G)[idx])){

    
        
    x$GuideSeq <- as.character(G[idx])
    
    x$RetrievedPos <- sapply(x$GuideSeq ,GetMatch, y = FASTA, VAL = "pos" , MM=1, TARGET = target)
    

    
    if (all(x$MatchPos == x$RetrievedPos)){
      
      TYPES <- split(x, f = x$MatchType)
      TYPES <- TYPES[grep("Non-Targeting",names(TYPES), invert = T)]
      all <- x[grep("rc_",x$GuideName, invert = T),]
      TYPES = c(list(all),TYPES)
      names(TYPES)[1] <- "all"
      
      types <- names(TYPES)
      
      dat = TYPES[[TYPE]]
      
      dat <- dat[order(dat$meanCS, decreasing = T),]
      del = which(is.na(dat$meanCS) == T)
      if(length(del) > 0){
        dat <- dat[-del,]
      }
      

      
      dat$TargetSeq <- sapply(dat$GuideSeq ,GetMatch, y = FASTA, VAL = "TargetSeq" , MM=1 , TARGET = target )
      
      if (PERSPECTIVE == "target"){
        
        # split the retrieved sequence by nucleotide. The sequence has -4 and +4 nucleotides surrounding the Cas13d guide match. The sequence represents the target sequence, that is the reverse complement of the guide sequence 
        ma <- as.data.frame(do.call(rbind,sapply(dat$TargetSeq, FUN = function(y){strsplit(y,split="")}))) # produces a warning, that can be ignored, and will be corrected downstream
        cat("\nproduces a warning, that can be ignored, and will be corrected downstream\n\n")
        colnames(ma) <- c("-4","-3","-2","-1",seq(guideLen,1,-1),"+1","+2","+3","+4")
        rownames(ma) <- rownames(dat) # add rownames
        ma$matchPos  <- dat$MatchPos # add match positions 
        ma$CS  <- dat$meanCS # add CRISPR score
        
        

        offset = 4 # set length of offset surrounding the guide
        WINDOW = (guideLen+(2*offset))
        SeqLen = width(FASTA)
        
        # As some guide may fall at the target edges, we need to correct the that by assigning NAs to upstream and downstream regions
        # initialize output matrix
        out = as.data.frame(matrix(NA, ncol = WINDOW, nrow = nrow(dat) ))
        colnames(out) =  colnames(ma)[1:WINDOW]
        rownames(out) = rownames(ma)
        out$matchPos  <- ma$matchPos
        out$CS  <- ma$CS
        ma <- as.matrix(ma)
        out <- as.matrix(out)
        
        # correct nucleotide positions of target sites for target site falling close to target edges
        for ( i in 1:nrow(ma)){
          
          # if close to target start, move the sequence 'to the right', and add NAs to the first positions, if non-matching
          if (as.numeric(ma[i,"matchPos"]) < (guideLen + offset)){
            
            o = ( guideLen + offset) - as.numeric(ma[i,"matchPos"])
            
            out[i,((o+1):(offset + guideLen + offset) )] <- ma[i,(1:(as.numeric(ma[i,"matchPos"])+offset))]
            
            ma[i, ] <- out[i,]
            
          }
          # if close to target end, no move of the sequence required, but add NAs to the last positions, if non-matching
          else if (as.numeric(ma[i,"matchPos"]) > (SeqLen-offset)){
            
            o = (as.numeric(ma[i,"matchPos"])+offset) -  SeqLen
            
            ma[i,(offset + guideLen + offset - (o-1)):(offset + guideLen + offset) ] <- NA
            
            
          }
          else{
            next
          }
        }
        
        remove(out) # remove intermediate file
        ma <- as.data.frame(ma) # transform to data frame
        ma$CS <- as.numeric(as.character(ma$CS)) # make sure the CS score is numeric
        
        # order the selected guide by their CS score
        ma = ma[order(as.numeric(as.character(ma$CS)), decreasing = T),]
        
        if(DATA){
          return(ma)
        }
        else{
          
          # assign a set containing all
          all <- ma
          # assigning a set containing the top 20%
          top20 <- ma[which(ma$CS >= quantile(as.numeric(as.character(ma$CS)), prob = 0.8)),]
          # bottom <- ma[which(ma$CS < quantile(as.numeric(as.character(ma$CS)), prob = 0.8)),]
          
          
          # get p values for nucloetides being disfavored using a binomial distribution (not bonferroni corrected!!!)
          p.disfavored = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW], LowerTail = TRUE, MultipleTestingCor = F, EffectSize = FALSE)
          # get p values for nucloetides being favored using a binomial distribution (not bonferroni corrected!!!)
          p.favored = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW], LowerTail = FALSE, MultipleTestingCor = F, EffectSize = FALSE)   
          # get adjusted p values for nucloetides being disfavored using a binomial distribution (bonferroni corrected)
          p.disfavored.bf = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW], LowerTail = TRUE, MultipleTestingCor = T, EffectSize = FALSE)
          # get adjusted p values for nucloetides being favored using a binomial distribution (bonferroni corrected)
          p.favored.bf = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW], LowerTail = FALSE, MultipleTestingCor = T, EffectSize = FALSE)
          # get the effect size for nucloetides being disfavored/favored using the ratio of probabilities
          effectSize = log2(Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW],  EffectSize = TRUE))
          # get the effect size for nucloetides being disfavored - favored using the differnce of probabilities
          deltaProb = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW],  EffectSize = TRUE, MEASURE = "deltaFC" )
          
          
          # plot delta probability 
          xlbs <- colnames(deltaProb) # keep column name info, because R interpretes -1 and +1 numerically
          colnames(deltaProb) <- seq(1,length(xlbs),1) # assign surrogate column names not to confuse R
          df = melt(deltaProb) # turn the matrix into a df for usage of ggplots
          colnames(df) <- c("NT", "Pos","DeltaProbability") # rename columns
          df$Pos <- rep(xlbs,  each = 4 ) # assign the original Columnnames to get the relative positions
          df$Pos <- factor(df$Pos, levels = xlbs) # factorize the position
          df$NT <- factor(df$NT, levels = c("A","C","G","U")) # factorize the NT
          #plot
          p.deltaProb = ggplot( data = df, aes(x=Pos, y=DeltaProbability , fill = NT)) + geom_bar(stat="identity", position=position_dodge()) + theme_classic() + scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) + ylim(-0.3,0.3) + ylab("delta probability") + xlab("guide position") +
            geom_vline(xintercept=seq(2,WINDOW,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) + coord_fixed(WINDOW/2.4) + ggtitle("Delta Probability - nt prevalence in top20% vs all perfect matching guides") +
            annotate( geom = "text" , y = -0.1, x = 1 , label = "disfavored" , angle = -90, hjust = "inward", color = "darkgrey", size = 3 ) +
            annotate( geom = "text" , y = 0.1, x = 1 , label = "favored" , angle = 90, hjust = "inward", color = "darkgrey", size = 3 )
          
          
          
          
          
          
          
          
          
          # plot Effect Size 
          xlbs <- colnames(effectSize) # keep column name info, because R interpretes -1 and +1 numerically
          colnames(effectSize) <- seq(1,length(xlbs),1) # assign surrogate column names not to confuse R
          df = melt(effectSize) # turn the matrix into a df for usage of ggplots
          colnames(df) <- c("NT", "Pos","EffectSize") # rename columns
          df$Pos <- rep(xlbs,  each = 4 ) # assign the original Columnnames to get the relative positions
          df$Pos <- factor(df$Pos, levels = xlbs) # factorize the position
          df$NT <- factor(df$NT, levels = c("A","C","G","U")) # factorize the NT
          #plot
          p.effectsize = ggplot( data = df, aes(x=Pos, y=EffectSize , fill = NT)) + geom_bar(stat="identity", position=position_dodge()) + theme_classic() + scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) + ylim(-2,2) + ylab("log2(FC)") + xlab("guide position") +
            geom_vline(xintercept=seq(2,WINDOW,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) + coord_fixed(WINDOW/16) + ggtitle("Effect size - nt prevalence in top20% vs all perfect matching guides") +
            annotate( geom = "text" , y = -0.5, x = 1 , label = "disfavored" , angle = -90, hjust = "inward", color = "darkgrey", size = 3 ) +
            annotate( geom = "text" , y = 0.5, x = 1 , label = "favored" , angle = 90, hjust = "inward", color = "darkgrey", size = 3 )
          
          
          
          # plot the corresponding pValues
          p = p.disfavored
          p[effectSize < 0] <- 1
          p[effectSize >= 0] <- 1
          for ( i in 1:ncol(effectSize)){
            for (k in 1:nrow(effectSize)){
              if (effectSize[k,i] > 0){
                p[k,i] <- -log10(p.favored[k,i])
              }
              else{
                p[k,i] <- log10(p.disfavored[k,i])
              }
            }
          }
          
          
          
          xlbs <- colnames(p)
          colnames(p) <- seq(1,length(xlbs),1)
          df = melt(p)
          colnames(df) <- c("NT", "Pos","pVal")
          df$Pos <- rep(xlbs,  each = 4 )
          df$Pos <- factor(df$Pos, levels = xlbs)
          df$NT <- factor(df$NT, levels = c("A","C","G","U"))
          p.pVal = ggplot( data = df, aes(x=Pos, y=pVal , fill = NT)) + geom_bar(stat="identity", position=position_dodge()) + theme_classic() + scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) + ylim(-10,10) + ylab("-log10(p)") + xlab("guide position") +
            geom_vline(xintercept=seq(2,WINDOW,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) + coord_fixed(WINDOW/80) + ggtitle("P - nt prevalence in top20% vs all perfect matching guides") +
            annotate( geom = "text" , y = -1.5, x = 1 , label = "disfavored" , angle = -90, hjust = "inward", color = "darkgrey", size = 3 ) +
            annotate( geom = "text" , y = 1.5, x = 1 , label = "favored" , angle = 90, hjust = "inward", color = "darkgrey", size = 3 )
          
          
          
          
          
          
          
          
          # plot the corresponding pValues
          p = p.disfavored.bf
          p[effectSize < 0] <- 1
          p[effectSize >= 0] <- 1
          for ( i in 1:ncol(effectSize)){
            for (k in 1:nrow(effectSize)){
              if (effectSize[k,i] > 0){
                p[k,i] <- -log10(p.favored.bf[k,i])
              }
              else{
                p[k,i] <- log10(p.disfavored.bf[k,i])
              }
            }
          }
          
          
          
          xlbs <- colnames(p)
          colnames(p) <- seq(1,length(xlbs),1)
          df = melt(p)
          colnames(df) <- c("NT", "Pos","pVal")
          df$Pos <- rep(xlbs,  each = 4 )
          df$Pos <- factor(df$Pos, levels = xlbs)
          df$NT <- factor(df$NT, levels = c("A","C","G","U"))
          p.pVal.bf = ggplot( data = df, aes(x=Pos, y=pVal , fill = NT)) + geom_bar(stat="identity", position=position_dodge()) + theme_classic() + scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) + ylim(-10,10) + ylab("-log10(p.adj)") + xlab("guide position") +
            geom_vline(xintercept=seq(2,WINDOW,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) + coord_fixed(WINDOW/80) + ggtitle("adjusted P - nt prevalence in top20% vs all perfect matching guides") +
            annotate( geom = "text" , y = -1.5, x = 1 , label = "disfavored" , angle = -90, hjust = "inward", color = "darkgrey", size = 3 ) +
            annotate( geom = "text" , y = 1.5, x = 1 , label = "favored" , angle = 90, hjust = "inward", color = "darkgrey", size = 3 )
          
          
          
          
          
          # par(mfrow = c(2,4))
          # gbinom(sum(background[,1]), prob.A,  scale = T)
          # gbinom(sum(background[,1]), prob.C,  scale = T)
          # gbinom(sum(background[,1]), prob.G,  scale = T)
          # gbinom(sum(background[,1]), prob.T,  scale = T)
          # 
          # gbinom(sum(top[,1]), prob.A,  scale = T)
          # gbinom(sum(top[,1]), prob.C,  scale = T)
          # gbinom(sum(top[,1]), prob.G,  scale = T)
          # gbinom(sum(top[,1]), prob.T,  scale = T)
          # 
          # 
          # pbinom(top[1,1], size=sum(top[,1]), prob = prob.A, lower.tail = F, log.p = F)
          
          p.list = list()
          p.list[[1]] <- p.deltaProb
          p.list[[2]] <- p.effectsize
          p.list[[3]] <- p.pVal
          p.list[[4]] <- p.pVal.bf
          return(p.list)          
          
        }
        
        
        
      }
      
      else{
        stop("the corresponding guide perspective hasn't been implemented yet")
      }
      
      return(g)
      
    }
    else{
      stop("not all guides match positions are identical")
    }
    
  }
  else{
    stop("not all guides match")
  }
}


########################## sub-routines end  ########

###### execute ######################################




# Read combined result table
Results = read.delim('./data/CombinedTilingScreenResults.csv' , sep = ',', header = T, stringsAsFactors = F)

# Only use CDS annotating guides (can be scwitched off)
Results = Results[grep("CDS",Results$Annotation),]

# Restrict to GFP Screen
Screens = split(Results, f = Results$Screen)
GFP = Screens[["GFP"]]
CD46 = Screens[["CD46"]]
CD55 = Screens[["CD55"]]
CD71 = Screens[["CD71"]]




# Get Target Sequence
gfp.fa = "./PreProcessingMetaData/data/GFP.fa"
cd46.fa = "./PreProcessingMetaData/data/CD46_ENST00000367042.1.fa"
cd55.fa = "./PreProcessingMetaData/data/CD55_ENST00000367064.3.fa"
cd71.fa = "./PreProcessingMetaData/data/CD71_ENST00000360110.4.fa"
FA.GFP <- Biostrings::readDNAStringSet(filepath = gfp.fa, format = "fasta", use.names = T)
FA.CD46 <- Biostrings::readDNAStringSet(filepath = cd46.fa, format = "fasta", use.names = T)
FA.CD55 <- Biostrings::readDNAStringSet(filepath = cd55.fa, format = "fasta", use.names = T)
FA.CD71 <- Biostrings::readDNAStringSet(filepath = cd71.fa, format = "fasta", use.names = T)





# Get Guide Sequences
gfp.fa = "./PreProcessingMetaData/data/Cas13d_GFP_library.final.fa"
cd46.fa = "./PreProcessingMetaData/data/CD46_library.final.fa"
cd55.fa = "./PreProcessingMetaData/data/CD55_library.final.fa"
cd71.fa = "./PreProcessingMetaData/data/CD71_library.final.fa"
GUIDES.GFP  <- Biostrings::readDNAStringSet(filepath = gfp.fa, format = "fasta", use.names = T)
GUIDES.CD46  <- Biostrings::readDNAStringSet(filepath = cd46.fa, format = "fasta", use.names = T)
GUIDES.CD55  <- Biostrings::readDNAStringSet(filepath = cd55.fa, format = "fasta", use.names = T)
GUIDES.CD71  <- Biostrings::readDNAStringSet(filepath = cd71.fa, format = "fasta", use.names = T)







# Assess nt preference
NT.gfp = Assess_nt_preference(x = GFP , G = GUIDES.GFP , FASTA=FA.GFP , TYPE = "Perfect Match" ,  PERSPECTIVE = "target", guideLen = 27)
NT.cd46 = Assess_nt_preference(x = CD46 , G = GUIDES.CD46 , FASTA=FA.CD46 , TYPE = "Perfect Match" ,  PERSPECTIVE = "target", guideLen = 23)
NT.cd55 = Assess_nt_preference(x = CD55 , G = GUIDES.CD55 , FASTA=FA.CD55 , TYPE = "Perfect Match" ,  PERSPECTIVE = "target", guideLen = 23)
NT.cd71 = Assess_nt_preference(x = CD71 , G = GUIDES.CD71 , FASTA=FA.CD71 , TYPE = "Perfect Match" ,  PERSPECTIVE = "target", guideLen = 23)


pdf("./figures/Note1Fig2a_NTpreference_GFP.pdf", width = 7, height = 10, useDingbats = F)
grid.arrange(arrangeGrob(grobs= NT.gfp,ncol=1))
dev.off()
pdf("./figures/Note2Fig2a_NTpreference_CD46.pdf", width = 7, height = 10, useDingbats = F)
grid.arrange(arrangeGrob(grobs= NT.cd46,ncol=1))
dev.off()
pdf("./figures/Note2Fig2a_NTpreference_CD55.pdf", width = 7, height = 10, useDingbats = F)
grid.arrange(arrangeGrob(grobs= NT.cd55,ncol=1))
dev.off()
pdf("./figures/Note2Fig2a_NTpreference_CD71.pdf", width = 7, height = 10, useDingbats = F)
grid.arrange(arrangeGrob(grobs= NT.cd71,ncol=1))
dev.off()


############################### All combined

# extract data from each protein
dat.gfp  = Assess_nt_preference(x = GFP  , G = GUIDES.GFP  , FASTA=FA.GFP  , TYPE = "Perfect Match" ,  PERSPECTIVE = "target", guideLen = 27, DATA = TRUE)
dat.cd46 = Assess_nt_preference(x = CD46 , G = GUIDES.CD46 , FASTA=FA.CD46 , TYPE = "Perfect Match" ,  PERSPECTIVE = "target", guideLen = 23, DATA = TRUE)
dat.cd55 = Assess_nt_preference(x = CD55 , G = GUIDES.CD55 , FASTA=FA.CD55 , TYPE = "Perfect Match" ,  PERSPECTIVE = "target", guideLen = 23, DATA = TRUE)
dat.cd71 = Assess_nt_preference(x = CD71 , G = GUIDES.CD71 , FASTA=FA.CD71 , TYPE = "Perfect Match" ,  PERSPECTIVE = "target", guideLen = 23, DATA = TRUE)

# scale data
dat.gfp$ScaledCS = scale(dat.gfp$CS , center = T)[,1]
dat.cd46$ScaledCS = scale(dat.cd46$CS , center = T)[,1]
dat.cd55$ScaledCS = scale(dat.cd55$CS , center = T)[,1]
dat.cd71$ScaledCS = scale(dat.cd71$CS , center = T)[,1]

# select top percent for each one
dat.gfp$SelectedTopN = ifelse( dat.gfp$ScaledCS > quantile(dat.gfp$ScaledCS , probs = 0.8), "top", NA)
dat.cd46$SelectedTopN = ifelse( dat.cd46$ScaledCS > quantile(dat.cd46$ScaledCS , probs = 0.8), "top", NA)
dat.cd55$SelectedTopN = ifelse( dat.cd55$ScaledCS > quantile(dat.cd55$ScaledCS , probs = 0.8), "top", NA)
dat.cd71$SelectedTopN = ifelse( dat.cd71$ScaledCS > quantile(dat.cd71$ScaledCS , probs = 0.8), "top", NA)
dat.gfp$SelectedTopN = ifelse( dat.gfp$ScaledCS < quantile(dat.gfp$ScaledCS , probs = 0.2), "bot", dat.gfp$SelectedTopN)
dat.cd46$SelectedTopN = ifelse( dat.cd46$ScaledCS < quantile(dat.cd46$ScaledCS , probs = 0.2), "bot", dat.cd46$SelectedTopN)
dat.cd55$SelectedTopN = ifelse( dat.cd55$ScaledCS < quantile(dat.cd55$ScaledCS , probs = 0.2), "bot", dat.cd55$SelectedTopN)
dat.cd71$SelectedTopN = ifelse( dat.cd71$ScaledCS < quantile(dat.cd71$ScaledCS , probs = 0.2), "bot", dat.cd71$SelectedTopN)

# GFP is 27 nt in Guide Length, CD46, CD55 and CD71 are 23 nt in Guide Length
# Since positions 23-27 do not contribute to binding (see nucleotide mismatches in that region), i consider GFP guides only 23 nt long
# transform gfp data
del = which( colnames( dat.gfp ) %in% c("-4","-3","-2","-1"))
if (length(del) > 0){
  dat.gfp = dat.gfp[,-del]
  colnames(dat.gfp)[1:4] = c("-4","-3","-2","-1")
}

ma = rbind.data.frame(dat.gfp, dat.cd46, dat.cd55, dat.cd71)
ma$ScaledCS.all = scale(ma$CS , center = T)[,1]





# assign a set containing all
all <- ma
# assigning a set containing the top 20% of each screen
top20 <- ma[ which( ma$SelectedTopN == "top"),]
# bottom <- ma[which(ma$CS < quantile(as.numeric(as.character(ma$CS)), prob = 0.8)),]

guideLen = 23
offset = 4 # set length of offset surrounding the guide
WINDOW = (guideLen+(2*offset))

# get p values for nucloetides being disfavored using a binomial distribution (not bonferroni corrected!!!)
p.disfavored = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW], LowerTail = TRUE, MultipleTestingCor = F, EffectSize = FALSE)
# get p values for nucloetides being favored using a binomial distribution (not bonferroni corrected!!!)
p.favored = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW], LowerTail = FALSE, MultipleTestingCor = F, EffectSize = FALSE)   
# get adjusted p values for nucloetides being disfavored using a binomial distribution (bonferroni corrected)
p.disfavored.bf = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW], LowerTail = TRUE, MultipleTestingCor = T, EffectSize = FALSE)
# get adjusted p values for nucloetides being favored using a binomial distribution (bonferroni corrected)
p.favored.bf = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW], LowerTail = FALSE, MultipleTestingCor = T, EffectSize = FALSE)
# get the effect size for nucloetides being disfavored/favored using the ratio of probabilities
effectSize = log2(Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW],  EffectSize = TRUE))
# get the effect size for nucloetides being disfavored - favored using the differnce of probabilities
deltaProb = Get_p_binomal(TOP = top20[,1:WINDOW], BACKGROUND = all[,1:WINDOW],  EffectSize = TRUE, MEASURE = "deltaFC" )


# plot delta probability 
xlbs <- colnames(deltaProb) # keep column name info, because R interpretes -1 and +1 numerically
colnames(deltaProb) <- seq(1,length(xlbs),1) # assign surrogate column names not to confuse R
df = melt(deltaProb) # turn the matrix into a df for usage of ggplots
colnames(df) <- c("NT", "Pos","DeltaProbability") # rename columns
df$Pos <- rep(xlbs,  each = 4 ) # assign the original Columnnames to get the relative positions
df$Pos <- factor(df$Pos, levels = xlbs) # factorize the position
df$NT <- factor(df$NT, levels = c("A","C","G","U")) # factorize the NT
#plot
p.deltaProb = ggplot( data = df, aes(x=Pos, y=DeltaProbability , fill = NT)) + geom_bar(stat="identity", position=position_dodge()) + theme_classic() + scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) + ylim(-0.15,0.15) + ylab("delta probability") + xlab("guide position") +
  geom_vline(xintercept=seq(2,WINDOW,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) + coord_fixed(WINDOW/1.2) + ggtitle("Delta Probability - nt prevalence in top20% vs all perfect matching guides") +
  annotate( geom = "text" , y = -0.05, x = 1 , label = "disfavored" , angle = -90, hjust = "inward", color = "darkgrey", size = 3 ) +
  annotate( geom = "text" , y = 0.05, x = 1 , label = "favored" , angle = 90, hjust = "inward", color = "darkgrey", size = 3 )









# plot Effect Size 
xlbs <- colnames(effectSize) # keep column name info, because R interpretes -1 and +1 numerically
colnames(effectSize) <- seq(1,length(xlbs),1) # assign surrogate column names not to confuse R
df = melt(effectSize) # turn the matrix into a df for usage of ggplots
colnames(df) <- c("NT", "Pos","EffectSize") # rename columns
df$Pos <- rep(xlbs,  each = 4 ) # assign the original Columnnames to get the relative positions
df$Pos <- factor(df$Pos, levels = xlbs) # factorize the position
df$NT <- factor(df$NT, levels = c("A","C","G","U")) # factorize the NT
#plot
p.effectsize = ggplot( data = df, aes(x=Pos, y=EffectSize , fill = NT)) + geom_bar(stat="identity", position=position_dodge()) + theme_classic() + scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) + ylim(-0.75,0.75) + ylab("log2(FC)") + xlab("guide position") +
  geom_vline(xintercept=seq(2,WINDOW,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) + coord_fixed(WINDOW/6) + ggtitle("Effect size - nt prevalence in top20% vs all perfect matching guides") +
  annotate( geom = "text" , y = -0.25, x = 1 , label = "disfavored" , angle = -90, hjust = "inward", color = "darkgrey", size = 3 ) +
  annotate( geom = "text" , y = 0.25, x = 1 , label = "favored" , angle = 90, hjust = "inward", color = "darkgrey", size = 3 )



# plot the corresponding pValues
p = p.disfavored
p[effectSize < 0] <- 1
p[effectSize >= 0] <- 1
for ( i in 1:ncol(effectSize)){
  for (k in 1:nrow(effectSize)){
    if (effectSize[k,i] > 0){
      p[k,i] <- -log10(p.favored[k,i])
    }
    else{
      p[k,i] <- log10(p.disfavored[k,i])
    }
  }
}



xlbs <- colnames(p)
colnames(p) <- seq(1,length(xlbs),1)
df = melt(p)
colnames(df) <- c("NT", "Pos","pVal")
df$Pos <- rep(xlbs,  each = 4 )
df$Pos <- factor(df$Pos, levels = xlbs)
df$NT <- factor(df$NT, levels = c("A","C","G","U"))
p.pVal = ggplot( data = df, aes(x=Pos, y=pVal , fill = NT)) + geom_bar(stat="identity", position=position_dodge()) + theme_classic() + scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) + ylim(-15,15) + ylab("-log10(p)") + xlab("guide position") +
  geom_vline(xintercept=seq(2,WINDOW,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) + coord_fixed(WINDOW/120) + ggtitle("P - nt prevalence in top20% vs all perfect matching guides") +
  annotate( geom = "text" , y = -1.5, x = 1 , label = "disfavored" , angle = -90, hjust = "inward", color = "darkgrey", size = 3 ) +
  annotate( geom = "text" , y = 1.5, x = 1 , label = "favored" , angle = 90, hjust = "inward", color = "darkgrey", size = 3 )








# plot the corresponding pValues
p = p.disfavored.bf
p[effectSize < 0] <- 1
p[effectSize >= 0] <- 1
for ( i in 1:ncol(effectSize)){
  for (k in 1:nrow(effectSize)){
    if (effectSize[k,i] > 0){
      p[k,i] <- -log10(p.favored.bf[k,i])
    }
    else{
      p[k,i] <- log10(p.disfavored.bf[k,i])
    }
  }
}



xlbs <- colnames(p)
colnames(p) <- seq(1,length(xlbs),1)
df = melt(p)
colnames(df) <- c("NT", "Pos","pVal")
df$Pos <- rep(xlbs,  each = 4 )
df$Pos <- factor(df$Pos, levels = xlbs)
df$NT <- factor(df$NT, levels = c("A","C","G","U"))
p.pVal.bf = ggplot( data = df, aes(x=Pos, y=pVal , fill = NT)) + geom_bar(stat="identity", position=position_dodge()) + theme_classic() + scale_fill_manual(values=c("#41ab5d", "#4292c6", "#fe9929","#cb181d")) + ylim(-10,10) + ylab("-log10(p.adj)") + xlab("guide position") +
  geom_vline(xintercept=seq(2,WINDOW,1)-0.5, linetype="solid", color = "#bdbdbd", size=0.1) + coord_fixed(WINDOW/80) + ggtitle("adjusted P - nt prevalence in top20% vs all perfect matching guides") +
  annotate( geom = "text" , y = -1.5, x = 1 , label = "disfavored" , angle = -90, hjust = "inward", color = "darkgrey", size = 3 ) +
  annotate( geom = "text" , y = 1.5, x = 1 , label = "favored" , angle = 90, hjust = "inward", color = "darkgrey", size = 3 )




pdf("./figures/Note2Fig2a_NTpreference_All.pdf", width = 7, height = 10, useDingbats = F)
grid.arrange(arrangeGrob(grobs = list( p.deltaProb, p.effectsize , p.pVal , p.pVal.bf)   ,ncol=1))
dev.off()
