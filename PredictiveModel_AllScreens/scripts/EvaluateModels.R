#!/usr/bin/env Rscript

args = commandArgs(trailingOnly=TRUE)

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressWarnings(suppressMessages(library(dplyr, quietly = T)))
suppressWarnings(suppressMessages(library(reshape2, quietly = T)))
suppressWarnings(suppressMessages(library(ggplot2, quietly = T)))
suppressWarnings(suppressMessages(library(robustbase, quietly = T)))

Version = as.character(args[1]) # string Version = "v3"


###### sub-routines #################################

########################## sub-routines end  ########

###### execute ######################################

Processed.Files = c(paste0("./data/Learning_approaches_Rsqured_",Version,".csv"), paste0("./data/Learning_approaches_Spearman_",Version,".csv"), paste0("./data/Features_",Version,".RData"))


if( !all(file.exists(Processed.Files)) ){
  
  cat("Files not found:", Processed.Files, "\n")
  cat(paste0("Loading from expected regression_models_Cas13_",Version,".R output \n"))
  
  # load files
  files = list.files( path = paste0("tmp_ModelData_",Version), full.names =  T, pattern = "*.txt")
  if (length(files) == 0)(
    stop(paste0("Exiting! No files found. Please run regression_models_Cas13_",Version,".R first!"))
  )
  Model.rho.files = files[grep("Model.rho", files)] 
  Model.sp.files = files[grep("Model.sp", files)] 
  Feature.files = files[grep("FeatureSelection", files)] 
  
  ########### Model performance r^2
  a = do.call(rbind,lapply(Model.rho.files, FUN=function(x){read.table(x, sep = "\t")}))
  a = unique(a)
  a = na.omit(a)
  a=a^2
  a = as.data.frame(a)
  a = a[,order(colMedians(as.matrix(a)), decreasing = F)]
  colnames(a) = gsub("SVM.tuning.radbias","SVM+tuning",colnames(a))
  write.table(x = round(a,6), file = paste0("./data/Learning_approaches_Rsqured_",Version,".csv"), quote = F, col.names = T, row.names = F, sep = ",")
  
  ########### Model performance spearman
  b = do.call(rbind,lapply(Model.sp.files, FUN=function(x){read.table(x, sep = "\t")}))
  b = unique(b)
  b = na.omit(b)
  b = as.data.frame(b)
  b = b[,order(colMedians(as.matrix(b)), decreasing = F)]
  colnames(b) = gsub("SVM.tuning.radbias","SVM+tuning",colnames(b))
  
  write.table(x = round(b,4), file = paste0("./data/Learning_approaches_Spearman_",Version,".csv"), quote = F, col.names = T, row.names = F, sep = ",")
  
  ########### Features
  Features = lapply(Feature.files, FUN=function(x){read.table(x)[,-1]})
  save(Features , file = paste0("./data/Features_",Version,".RData"))
  
}else{
  
  cat("loading pre-computed files\n")
  
  a <- read.csv(Processed.Files[1])
  b <- read.csv(Processed.Files[2])
  load(paste0("./data/Features_",Version,".RData")) # loads object called "Features"
}




########### Plot Model performance r^2


df = suppressMessages(melt(a))
colnames(df) = c("Method","r.squared")
df$Approach = ifelse( grepl("ontext" , df$Method)  , "Linear Model" , "Learning Approach")
df$Approach = factor( df$Approach , levels = c("Linear Model" , "Learning Approach"))

medians = aggregate(r.squared ~  Method, df, median)

pdf(paste0("./figures/Model_Performance_r2_",Version,".pdf"), width = 8, height = 4, useDingbats = F)
ggplot(data = df, aes( x = Method , y = r.squared)) +
  geom_boxplot( outlier.size = 0.1,  aes(fill = Approach)) +
  scale_fill_manual( values = c( "#3182bd","#de2d26")) +
  theme_classic() +
  ylab(expression(paste("r"^"2", " to held-out data"))) +
  xlab("") +
  ylim(c(-0.05,0.52))+
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  geom_text(data = medians, aes(label = round(r.squared,2), y = r.squared + 0.02), colour = "white", size=2.5) +
  coord_fixed(ratio=10)
dev.off()






########### Plot Model performance Spearman

df = suppressMessages(melt(b))
colnames(df) = c("Method","Spearman")
df$Approach = ifelse( grepl("ontext" , df$Method)  , "Linear Model" , "Learning Approach")
df$Approach = factor( df$Approach , levels = c("Linear Model" , "Learning Approach"))

medians = aggregate(Spearman ~  Method, df, median)


pdf(paste0("./figures/Model_Performance_spearman_",Version,".pdf"), width = 8, height = 4, useDingbats = F)
ggplot(data = df, aes( x = Method , y = Spearman)) +
  geom_boxplot( outlier.size = 0.1,  aes(fill = Approach)) +
  scale_fill_manual( values = c( "#3182bd","#de2d26")) +
  theme_classic() +
  ylab("spearmean coef. to held-out data") +
  xlab("") +
  #ylim(c(0.25,0.75))+
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  annotate("text", x = c(1:nrow(medians)), label = round(medians$Spearman,2), y = medians$Spearman , colour = "white", size=2.5 , vjust=0) 
  #coord_fixed(ratio = 9 / 0.5 / 1.5 )
dev.off()





########### Random Forest Feature importance

IncMSE = do.call(rbind,lapply(Features, FUN=function(x){x["randomForest.IncMSE",]}))
del = which( apply( as.matrix( IncMSE ) , MARGIN = 2 , FUN = function(x){max(x,na.rm=TRUE)}) == 0) 
if(length(del)>0){
  IncMSE = IncMSE[,-del]
}
IncMSE = IncMSE[,order(colMedians(as.matrix(IncMSE) , na.rm = T), decreasing = T)]
# IncNodePurity = do.call(rbind,lapply(Features, FUN=function(x){x["randomForest.IncNodePurity",]}))
# SVM.rad = do.call(rbind,lapply(Features, FUN=function(x){x["SVM.rad",]}))



write.table(x = round(IncMSE,4), file = paste0("./figures/RandomForest_FeatureImportance_",Version,".csv"), quote = F, col.names = T, row.names = F, sep = ",")



df = suppressMessages(melt(IncMSE))
colnames(df) = c("Feature","Value")
df$Feature = factor( df$Feature , levels = colnames(IncMSE)[order(colMedians(as.matrix(IncMSE)), decreasing = T)])

pdf(paste0("./figures/Model_Performace_RandomForest_weights_",Version,".pdf"), width = 4, height = 4, useDingbats = F)
ggplot(data = df, aes( x = Feature , y = Value)) +
  geom_boxplot( outlier.size = 0.1, fill = "#31a354") +
  theme_classic() +
  ylab("IncMSE") +
  xlab("Feature") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) 
dev.off()






########### Fwd Indiv Feature importance
# Features = lapply(Feature.files, FUN=function(x){read.table(x)[,-1]})
Fwd = do.call(rbind,lapply(Features, FUN=function(x){x["fwd-ind",]}))
StepBIC = do.call(rbind,lapply(Features, FUN=function(x){x["StepBIC",]}))
Lasso.min = do.call(rbind,lapply(Features, FUN=function(x){x["lambda.min",]}))
Lasso.1se = do.call(rbind,lapply(Features, FUN=function(x){x["lambda.1se",]}))




FeatSel = rbind(colSums(Fwd)/length(Features)*100,colSums(StepBIC)/length(Features)*100,colSums(Lasso.1se)/length(Features)*100,colSums(Lasso.min)/length(Features)*100)
del = which( apply( FeatSel , 2 , sum ) == 0)
if(length(del) > 0){
  FeatSel = FeatSel[,-del]
}
rownames(FeatSel) = c("fwd-indiv regsubsets", "StepBIC", "Lasso.min","Lasso.1se")
df = melt(FeatSel)
colnames(df) = c("Method","Feature","Percentage")

pdf(paste0("./figures/Model_Features_Lasso_StepBIC_",Version,".pdf"), width = 8, height = 4, useDingbats = F)
  ggplot( data = df, aes( x = Feature , y = Percentage, color = Method, shape = Method)) + theme_bw() +
    geom_point(size=rep(c(2.5,2,1.5,1), times=ncol(FeatSel))) + ylab("% feature was chosen") + xlab("") +
    theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
     scale_color_manual(values=c("#E69F00", "#009E73", "#56B4E9","#CC79A7"))
dev.off()







