#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
# install.packages(c("EnvStats","ggplot2","reshape2","ggridges","gridExtra","grid") , lib = "~/R/x86_64-redhat-linux-gnu-library/3.5", dependencies = T)
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(ggridges, quietly = T))
suppressMessages(library(EnvStats, quietly = T))
suppressMessages(library(gridExtra, quietly = T))
suppressMessages(library(grid, quietly = T))

GetTreatment = function(x){tmp = strsplit(x , split = "_")[[1]]
paste0(tmp[1:(length(tmp)-1)], collapse = "_")}

Assign_Guide_LENGTH <- function(x, ID = ID.mat, col = 2){
  
  row = which( ID[,1] == x)
  return(ID[row,col])
  
}

ExtractElement = function(x,y=1){
  strsplit( x , split = "_")[[1]][y]
}

Calculate_gMFI=function(a){
  m = abs(min(a))+1
  return(geoMean(a+m))}

SelectTopCells = function(x,p = P, side = "top"){
  if (side == "top"){
    sort(x,decreasing = T)[1:round(length(x)*p)]
  }else{
    sort(x,decreasing = F)[1:round(length(x)*p)]
  }
}

CalcPercGFPpos = function(x,Q=q){
  (table(x > Q)/length(x))["TRUE"]
}



###### execute ######################################
# load data
data = read.delim( file = "./FACS_data/FACS_GuideLength.csv", sep=",",  header = T,  stringsAsFactors = F)

ID.mat <- cbind(c("NT","NT_dup","Lipo","GFP2","GFP3","G58","G59","G60","GFP1","G61","G62","G63" ,    
                  "G64","G65","G66","G67","G68","GFP1_dup","G69","G70","G71","G72", "G73"  ),
                c("NT","NT","Lipo","GFP2","GFP3","50","40","33","30","27", "23","20", "17","14",
                  "50","40","33","30","27","23","20","17","14"),
                c("5p","3p",NA,NA,NA,"5p","5p","5p","5p","5p","5p","5p","5p", "5p",
                  "3p","3p","3p","3p","3p","3p","3p","3p","3p"))




#Plot histograms
dada = data[,grep("GFP2|GFP3|PUC|Lipo",colnames(data),invert = T)]
dada$GFP1_dup_1 = dada$GFP1_1 
dada$GFP1_dup_2 = dada$GFP1_2 
dada$GFP1_dup_3 = dada$GFP1_3 
dada$NT_dup_1 = dada$NT_1 
dada$NT_dup_2 = dada$NT_2 
dada$NT_dup_3 = dada$NT_3 
df = melt(dada)
df$Sample = sapply(as.character(df$variable) , GetTreatment)
df$Length = sapply( df$Sample , FUN = Assign_Guide_LENGTH, ID = ID.mat, col=2 )
df$Side = sapply( df$Sample , FUN = Assign_Guide_LENGTH, ID = ID.mat, col=3 )
df$Replicate = sapply(df$variable, FUN = function(z){strsplit(as.character(z), split = "_")[[1]][ length(strsplit(as.character(z), split = "_")[[1]]) ]})
df$Length = sapply( df$Sample , FUN = Assign_Guide_LENGTH, ID = ID.mat, col=2 )
df$Side = sapply( df$Sample , FUN = Assign_Guide_LENGTH, ID = ID.mat, col=3 )
df$Side <- factor(df$Side, levels = c("5p", "3p"))
df$Length <- factor(df$Length, levels = c("NT", "14", "17", "20", "23", "27", "30", "33", "40", "50"))
df$GuideR = paste0(df$Length ,"_",df$Replicate ) 


gg1 = ggplot(df, aes(x = value+1, y = Length)) +
  facet_wrap(~Side, ncol = 3)+
  geom_density_ridges(aes(fill = Length)) +
  scale_x_log10(limits=c(1,10000000)) +
  theme_classic()+
  ggtitle("spacer RNA length transfection summarized across replicates") +
  xlab("GFP signal") +
  ylab("spacer RNA length") + 
  scale_fill_manual(values = c("#969696", "#ffffbf", "#fee08b","#fdae61","#f46d43","#d53e4f", "#f46d43","#fdae61","#fee08b","#ffffbf"))

df$GuideR = factor(df$GuideR , levels = c("NT_1", "NT_2", "NT_3", "14_1", "14_2", "14_3","17_1", "17_2", "17_3","20_1", "20_2", "20_3","23_1", "23_2", "23_3","27_1", "27_2", "27_3","30_1", "30_2", "30_3","33_1", "33_2", "33_3","40_1", "40_2", "40_3","50_1", "50_2", "50_3")) 
gg2 = ggplot(df, aes(x = value+1, y = GuideR)) +
  facet_wrap(~Side, ncol = 3)+
  geom_density_ridges(aes(fill = Length)) +
  scale_x_log10(limits=c(1,10000000)) +
  theme_classic()+
  ggtitle("spacer RNA length transfection summarized across replicates") +
  xlab("GFP signal") +
  ylab("spacer RNA length") + 
  scale_fill_manual(values = c("#969696", "#ffffbf", "#fee08b","#fdae61","#f46d43","#d53e4f", "#f46d43","#fdae61","#fee08b","#ffffbf"))

pdf("./FACS_figures/QC_GuideLength_histograms.pdf", width = 10, height = 6, useDingbats = F)
grid.arrange(arrangeGrob(grobs= list(gg1,gg2) ,ncol=2))
dev.off()



# determine how many cells are GFP positive (esp. in NT control)
q = quantile(c(data[,grep("Lipo_1",colnames(data))],data[,grep("Lipo_2",colnames(data))],data[,grep("Lipo_3",colnames(data))]), probs = c(0.99))
PercGFPpos = apply(data , 2, CalcPercGFPpos) # determine how many cells got transfected and select the same percentage across all conditions assuming equal efficiency
P = mean(PercGFPpos[grep("NT",names(PercGFPpos))])
Transfected_cells = apply(data , 2, SelectTopCells, p = P, side = "top")




# Calculate MFI
data = Transfected_cells
data = cbind(data,rowMeans(data[,grep("NT",colnames(data))]))
colnames(data)[ncol(data)] = "NT"
# MFIs = colMeans(data) # colmeans to get average mfi per column
MFIs = apply(data, MARGIN = 2, Calculate_gMFI) # get geometric mean (less affected by outliers)
Reference = "NT"
output.mfi.percent  <- MFIs
for (i in 1:ncol(data)){
  output.mfi.percent[i] <- (MFIs[i]/MFIs[Reference])*100
}




# transform to data frame
df = melt(output.mfi.percent)
colnames(df)[1] <- "MFI"
df$name = names(output.mfi.percent)

# remove samples 
toRemove = c("NT",
             "PUC_1","PUC_2","PUC_3",
             "Lipo_1","Lipo_2","Lipo_3",
             "GFP2_1", "GFP2_2", "GFP2_3",
             "GFP3_1", "GFP3_2", "GFP3_3",
             "NT_1", "NT_2", "NT_3")
if(length(which(df$name %in% toRemove)) > 0){
  del = which(df$name %in%  toRemove)
  df = df[-del,]
}

df$Treatment = sapply( X = df$name ,  FUN=ExtractElement , y = 1 )
df$Replicate = sapply( X = df$name ,  FUN=ExtractElement , y = 2 )



ID.mat <- cbind(c("NT","Lipo","GFP2","GFP3","G58","G59","G60","GFP1","G61","G62","G63" ,    
                  "G64","G65","G66","G67","G68","GFP1_dup","G69","G70","G71","G72", "G73"  ),
                c("NT","Lipo","GFP2","GFP3","50","40","33","30","27", "23","20", "17","14",
                  "50","40","33","30","27","23","20","17","14"),
                c(NA,NA,NA,NA,"5p","5p","5p","5p","5p","5p","5p","5p", "5p",
                  "3p","3p","3p","3p","3p","3p","3p","3p","3p"))






# make it simpler
means = aggregate(MFI ~  Treatment, df, mean)
stdev = aggregate(MFI ~  Treatment, df, sd)[,2]
se = stdev/sqrt(3)
df = cbind.data.frame(means,stdev,se)
df = rbind.data.frame(df,df[df$Treatment == "GFP1",])
df$Treatment[length(df$Treatment)] = "GFP1_dup"
df$Length = sapply( df$Treatment , FUN = Assign_Guide_LENGTH, ID = ID.mat, col=2 )
df$Side = sapply( df$Treatment , FUN = Assign_Guide_LENGTH, ID = ID.mat, col=3 )
df$Side <- factor(df$Side, levels = c("5p", "3p"))



pdf("./FACS_figures/FigS1b_GuideLength.pdf", width = 5, height = 5, useDingbats = F)
ggplot( data = df, aes( x = Length, y = MFI , color = Side )) + 
  geom_smooth(aes(group=Side),method = 'loess' , span = 1, se=F,formula = 'y ~ x', color = c(rep("#3182bd",9),rep( "#de2d26", 9)) ) +
  scale_color_manual(values = c( "#3182bd","#de2d26")) +
  ylab("% GFP intensity rel. to ctrl") + 
  xlab("spacer length [nt]") +
  geom_errorbar( aes(ymin= MFI - se, ymax=  MFI + se),width=.2, position="identity" , colour = "#636363") + 
  geom_point( size = 2) +
  coord_fixed(4) +
  theme_bw() + 
  scale_y_continuous(trans='log10') +annotation_logticks(sides = "l")+
  theme(panel.grid.minor = element_blank())
dev.off()

