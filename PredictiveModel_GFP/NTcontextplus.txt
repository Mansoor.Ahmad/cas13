# Linear model NT-context+ calculated on the full on-target data set

Call:
lm(formula = meanCS.BIN1 ~ Local.A1 + Local.C + Local.G + Local.U + Local.upstreamU + MFE.crRNA, data = fullset)

Residuals:
     Min       1Q   Median       3Q      Max 
-1.10425 -0.26880  0.01348  0.28643  0.99311 

Coefficients:
                Estimate Std. Error t value Pr(>|t|)    
(Intercept)     -0.05471    0.11647  -0.470 0.638857    
Local.A1         0.24629    0.06887   3.576 0.000397 ***
Local.C         -0.32221    0.09322  -3.457 0.000613 ***
Local.G          0.15508    0.08809   1.760 0.079184 .  
Local.U          0.10641    0.06867   1.550 0.122099    
Local.upstreamU  0.21446    0.06224   3.446 0.000638 ***
MFE.crRNA        0.49438    0.09015   5.484 7.91e-08 ***
---
Signif. codes:  0 ‘***’ 0.001 ‘**’ 0.01 ‘*’ 0.05 ‘.’ 0.1 ‘ ’ 1

Residual standard error: 0.3784 on 356 degrees of freedom
Multiple R-squared:  0.3644,	Adjusted R-squared:  0.3537 
F-statistic: 34.02 on 6 and 356 DF,  p-value: < 2.2e-16