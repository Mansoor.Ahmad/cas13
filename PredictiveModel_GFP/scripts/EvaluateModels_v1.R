#!/usr/bin/env Rscript
# load libraries
suppressWarnings(suppressMessages(library(dplyr, quietly = T)))
suppressWarnings(suppressMessages(library(reshape2, quietly = T)))
suppressWarnings(suppressMessages(library(ggplot2, quietly = T)))
suppressWarnings(suppressMessages(library(robustbase, quietly = T)))


Processed.Files = c("Learning_approaches_Rsqured_v1.csv", "Learning_approaches_Spearman_v1.csv", "Features_v1.RData")


if( !all(file.exists(Processed.Files)) ){
  
  cat("Files not found:", Processed.Files, "\n")
  cat("Loading from expected regression_models_Cas13_v1.R output \n")
  
  # load files
  files = list.files(pattern = "*.txt")
  if (length(files) == 0)(
    stop("Exiting! No files found. Please run regression_models_Cas13_v1.R first!")
  )
  Model.rho.files = files[grep("Model.rho", files)] 
  Model.sp.files = files[grep("Model.sp", files)] 
  Feature.files = files[grep("FeatureSelection", files)] 
  
  ########### Model performance r^2
  a = do.call(rbind,lapply(Model.rho.files, FUN=function(x){read.table(x, sep = "\t")}))
  a = unique(a)
  a = na.omit(a)
  a=a^2
  a = as.data.frame(a)
  a = a[,order(colMedians(as.matrix(a)), decreasing = F)]
  colnames(a) = gsub("NT.context","NTcontext",gsub("SVM.tuning.radbias","SVM+tuning",colnames(a)))
  write.table(x = round(a,6), file = "Learning_approaches_Rsqured_v1.csv", quote = F, col.names = T, row.names = F, sep = ",")
  
  ########### Model performance spearman
  b = do.call(rbind,lapply(Model.sp.files, FUN=function(x){read.table(x, sep = "\t")}))
  b = unique(b)
  b = na.omit(b)
  b = as.data.frame(b)
  b = b[,order(colMedians(as.matrix(b)), decreasing = F)]
  colnames(b) = gsub("NT.context","NTcontext",gsub("SVM.tuning.radbias","SVM+tuning",colnames(b)))
  
  write.table(x = round(b,4), file = "Learning_approaches_Spearman_v1.csv", quote = F, col.names = T, row.names = F, sep = ",")
  
  ########### Features
  Features = lapply(Feature.files, FUN=function(x){read.table(x)[,-1]})
  save(Features , file = "Features_v1.RData")
  
}else{
  
  cat("loading pre-computed files\n")
  
  a <- read.csv(Processed.Files[1])
  b <- read.csv(Processed.Files[2])
  load("Features_v1.RData") # loads object called "Features"
}




########### Plot Model performance r^2


df = suppressMessages(melt(a))
colnames(df) = c("Method","r.squared")
df$Approach = ifelse(df$Method %in% c("Local.U","Local.G","upstream.U","Local.A", "Local.C", "NTcontext","MFE.crRNA","NTcontextPlus") , "Linear Model" , "Learning Approach")
df$Approach = factor( df$Approach , levels = c("Linear Model" , "Learning Approach"))

medians = aggregate(r.squared ~  Method, df, median)

pdf("Model_Performance_r2_v1.pdf", width = 8, height = 4, useDingbats = F)
ggplot(data = df, aes( x = Method , y = r.squared)) +
  geom_boxplot( outlier.size = 0.1,  aes(fill = Approach)) +
  scale_fill_manual( values = c( "#3182bd","#de2d26")) +
  theme_classic() +
  ylab(expression(paste("r"^"2", " to held-out data"))) +
  xlab("") +
  ylim(c(-0.05,0.52))+
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  geom_text(data = medians, aes(label = round(r.squared,2), y = r.squared + 0.02), colour = "white", size=2.5) +
  coord_fixed(ratio=10)
dev.off()






########### Plot Model performance Spearman

df = suppressMessages(melt(b))
colnames(df) = c("Method","Spearman")
df$Approach = ifelse(df$Method %in% c("Local.U","Local.G","upstream.U","Local.A", "Local.C", "NTcontext","MFE.crRNA","NTcontextPlus") , "Linear Model" , "Learning Approach")
df$Approach = factor( df$Approach , levels = c("Linear Model" , "Learning Approach"))

medians = aggregate(Spearman ~  Method, df, median)


pdf("Model_Performance_spearman_v1.pdf", width = 8, height = 4, useDingbats = F)
ggplot(data = df, aes( x = Method , y = Spearman)) +
  geom_boxplot( outlier.size = 0.1,  aes(fill = Approach)) +
  scale_fill_manual( values = c( "#3182bd","#de2d26")) +
  theme_classic() +
  ylab("spearmean coef. to held-out data") +
  xlab("") +
  #ylim(c(-0.12,0.72))+
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  geom_text(data = medians, aes(label = round(Spearman,2), y = Spearman + 0.025), colour = "white", size=2.5) +
  coord_fixed(ratio=10*0.7142857)
dev.off()





########### Random Forest Feature importance

IncMSE = do.call(rbind,lapply(Features, FUN=function(x){x["randomForest.IncMSE",]}))
IncMSE = IncMSE[,order(colMedians(as.matrix(IncMSE)), decreasing = T)]
# IncNodePurity = do.call(rbind,lapply(Features, FUN=function(x){x["randomForest.IncNodePurity",]}))
# SVM.rad = do.call(rbind,lapply(Features, FUN=function(x){x["SVM.rad",]}))

write.table(x = round(IncMSE,4), file = "RandomForest_FeatureImportance_v1.csv", quote = F, col.names = T, row.names = F, sep = ",")



df = suppressMessages(melt(IncMSE))
colnames(df) = c("Feature","Value")
df$Feature = factor( df$Feature , levels = colnames(IncMSE)[order(colMedians(as.matrix(IncMSE)), decreasing = T)])

pdf("Model_Performace_RandomForest_weights_v1.pdf", width = 2.5, height = 4, useDingbats = F)
ggplot(data = df, aes( x = Feature , y = Value)) +
  geom_boxplot( outlier.size = 0.1, fill = "#31a354") +
  theme_classic() +
  ylab("IncMSE") +
  xlab("Feature") +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
  coord_fixed(ratio=.5)
dev.off()






########### Fwd Indiv Feature importance
# Features = lapply(Feature.files, FUN=function(x){read.table(x)[,-1]})
Fwd = do.call(rbind,lapply(Features, FUN=function(x){x["fwd-ind",]}))
StepBIC = do.call(rbind,lapply(Features, FUN=function(x){x["StepBIC",]}))
Lasso.min = do.call(rbind,lapply(Features, FUN=function(x){x["lambda.min",]}))
Lasso.1se = do.call(rbind,lapply(Features, FUN=function(x){x["lambda.1se",]}))


FeatSel = rbind(colSums(Fwd)/1000*100,colSums(StepBIC)/1000*100,colSums(Lasso.1se)/1000*100,colSums(Lasso.min)/1000*100)
rownames(FeatSel) = c("fwd-indiv regsubsets", "StepBIC", "Lasso.min","Lasso.1se")
df = melt(FeatSel)
colnames(df) = c("Method","Feature","Percentage")

pdf("Model_Features_Lasso_StepBIC_v1.pdf", width = 8, height = 4, useDingbats = F)
  ggplot( data = df, aes( x = Feature , y = Percentage, color = Method, shape = Method)) + theme_bw() +
    geom_point(size=rep(c(2.5,2,1.5,1), times=ncol(FeatSel))) + ylab("% feature was chosen") + xlab("") +
    theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5)) +
     scale_color_manual(values=c("#E69F00", "#009E73", "#56B4E9","#CC79A7"))
dev.off()

