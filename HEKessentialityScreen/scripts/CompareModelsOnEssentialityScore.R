#!/usr/bin/env Rscript

###### set variables; load libs #####################
.libPaths(new="~/R/x86_64-redhat-linux-gnu-library/3.5")
suppressMessages(library(ggplot2, quietly = T))
suppressMessages(library(reshape2, quietly = T))
suppressMessages(library(grid, quietly = T))
suppressMessages(library(gridExtra, quietly = T))

###### sub-routines #################################

# get position of match start
extractPos <- function(y, END = "end"){
  coord <- strsplit( strsplit(y, split = ":")[[1]][2] ,split = "_")[[1]][1]
  if (END == "end" ){
    return(as.integer(strsplit(coord, split = "-")[[1]][2]))
  }
  else{
    return(as.integer(strsplit(coord, split = "-")[[1]][1]))
  }
}


getCor = function(dat , Y = "meanCS.D27", X = "standardizedGuideScores" , plot=FALSE , TITLE = '' , add=FALSE , METHOD = 'pearson'){
  if(plot){
    
    pcc = signif(cor( dat[,X] , dat[,Y] , use = "complete.obs", method = METHOD),3)
    
    g = ggplot(dat, aes(x=dat[,X] , dat[,Y] , color = Guide) )+
      geom_point(shape=20)+
      geom_smooth(method = 'lm' , se = FALSE , color = 'darkgrey') +
      theme_classic()+
      scale_color_manual(values = c("#de2d26","#3182bd"))+
      ggtitle( TITLE) +
      theme(plot.title = element_text(size = 10)) +
      annotate('text' , x = min(dat[,X] , na.rm = TRUE), y = min(dat[,Y] , na.rm = TRUE) , label = paste0(METHOD,' coef.: ',pcc ), hjust = 0, vjust=0)
    
    if(add){
     g = g + 
        xlab(paste("predicted",X)) +
        ylab(paste("observed",Y))
    }else{
      g = g + 
        xlab(paste("predicted")) +
        ylab(paste("observed"))
    }
    
    return(g)
    
  }
  else{
    return(cor( dat[,X] , dat[,Y] , use = "complete.obs" , method = METHOD ))
  }
  
}


########################## sub-routines end  ########

###### execute ######################################

# load data
ScreenResults = read.delim( 'data/HEK_screen_crRNA_enrichments.csv' , sep = ',' , header = T, stringsAsFactors = F )
ScreenResults$MatchPos <- sapply(rownames(ScreenResults),extractPos)

# retrieve guide scores from updated model
files = list.files(path = './data/TargetGenes' , full.names = TRUE , pattern = ".csv$")
Predictions = lapply(as.list(files), FUN = function(x){ read.delim(x , stringsAsFactors = FALSE, header = TRUE, sep=",")})
names(Predictions) = sapply( files , FUN = function(x){gsub( "_CasRxguides.csv" , "" , basename(x))} )

# add updated predictions
ids = na.omit(unique(ScreenResults$TranscriptID))
ScreenResults$NewGuideScores = NA
ScreenResults$NewStandardizedGuideScores = NA
targets = split(ScreenResults , f = ScreenResults$TranscriptID)
for( i in 1:length(ids)){
  id = ids[i]
  pred = Predictions[[id]]
  tar = targets[[id]]
  for ( j in 1:length(tar$MatchPos)){
    pos = tar$MatchPos[j] 
    idx = which( pred$MatchPos == pos)
    if (length(idx) == 0){
      next
    }
    else{
      tar$NewGuideScores[j] = pred$GuideScores[idx]
      tar$NewStandardizedGuideScores[j] = pred$standardizedGuideScores[idx]
    }
  }
  
  m = max(tar$meanCS.D27 )
  r = -1*(tar$meanCS.D27 - m)
  tar$PercentKnockdown = r/max(r) * 100


  
  
  targets[[id]] = tar
}
ScreenResults = do.call(rbind.data.frame , targets )

# restrict to essential genes
EG = ScreenResults[ which(ScreenResults$Class == "EG")  , ]
EGs = split(EG , f = EG$Gene)
ma = matrix(0,ncol = 2 , nrow = length(EGs))
rownames(ma) = names(EGs)
colnames(ma) = c("GFPmodel" , "CombinedModel" )
for(i in 1:length(EGs)){
  ma[i,"GFPmodel"]      = getCor(EGs[[i]] ,Y = "meanCS.D27", X = "standardizedGuideScores" , plot=FALSE , METHOD = 'pearson' )
  ma[i,"CombinedModel"] = getCor(EGs[[i]] ,Y = "meanCS.D27", X = "NewStandardizedGuideScores" , plot=FALSE , METHOD = 'pearson' )
}

gfp.model = list()
combined.model = list()
for(i in 1:length(EGs)){
  gfp.model[[i]]      = getCor(EGs[[i]] ,Y = "meanCS.D27", X = "standardizedGuideScores" , plot=TRUE , TITLE = paste0('GFP: ',names(EGs)[i]))
  combined.model[[i]] = getCor(EGs[[i]] ,Y = "meanCS.D27", X = "NewStandardizedGuideScores" , plot=TRUE , TITLE = paste0('Combined: ',names(EGs)[i]))
}


# pdf("./figures/ModelComparison_individualScatter.pdf", width = 30, height = 4, useDingbats = F)
# grid.arrange(arrangeGrob(grobs= c( gfp.model , combined.model) , nrow =2  ))
# dev.off()

df = melt(ma)
colnames(df) = c("Gene", "Model" , "PCC")
df$Gene = factor(df$Gene  , c( 'SNRNP200' , 'CCT7' , 'EIF3B' , 'PSMB6' , 'PSMB2', 'SFPQ', 'ARCN1' , 'NUP133' , 'SUPT5H' , 'PRPF19'    ) )
means = aggregate(PCC ~  Model, df, mean)
stdev = aggregate(PCC ~  Model, df, sd)
g1 = ggplot( df , aes( x = Gene , y = -1*(PCC) , fill = Model) )+
  geom_bar(stat='identity', position = 'dodge') + 
  theme_classic()+
  scale_fill_manual( values = c('#de2d26','#67000d') ) + 
  coord_fixed( ratio = 10/0.8) +
  theme(axis.text.x = element_text(angle = 90, hjust = 1, vjust = 0.5))+
  scale_y_continuous(breaks = seq(0,1,0.2)) +
  annotate('text' , x = 10 , y = 1 , label = paste0( "GFP: " ,-signif(means[1,2],2),"+/-",signif(stdev[1,2],2)), hjust=1) +
  annotate('text' , x = 10 , y = 0.925 , label = paste0( "Combined: " ,-signif(means[2,2],2),"+/-",signif(stdev[2,2],2)), hjust=1)+
  ylab("-1 * Person coef.")



g2 = getCor(dat = EG, Y = "meanCS.D27", X = "standardizedGuideScores" , plot=TRUE , TITLE = 'GFP model' , add = TRUE, METHOD = 'spearman')
g3 = getCor(dat = EG, Y = "meanCS.D27", X = "NewStandardizedGuideScores" , plot=TRUE , TITLE = 'Combined model' , add = TRUE, METHOD = 'spearman')

#g2 = getCor(dat = EG, Y = "meanCS.D27", X = "standardizedGuideScores" , plot=TRUE , TITLE = 'GFP model' , add = TRUE, METHOD = 'pearson')
#g3 = getCor(dat = EG, Y = "meanCS.D27", X = "NewStandardizedGuideScores" , plot=TRUE , TITLE = 'Combined model' , add = TRUE, METHOD = 'pearson')

g4 = getCor(dat = EG, Y = "PercentKnockdown", X = "standardizedGuideScores" , plot=TRUE , TITLE = 'GFP model' , add = TRUE, METHOD = 'spearman')
g5 = getCor(dat = EG, Y = "PercentKnockdown", X = "NewStandardizedGuideScores" , plot=TRUE , TITLE = 'Combined model' , add = TRUE, METHOD = 'spearman')


lay = rbind(c(1,3,5),
            c(2,4,NA))
pdf("./figures/ModelComparison.pdf", width = 12, height = 6, useDingbats = F)
grid.arrange(grobs = list(g2,g3,g4,g5,g1) , layout_matrix = lay)
dev.off()



# store results
out = rbind.data.frame(c('HEK', 'RFgfp' , 'meanCS.D27'),
                c('HEK', 'RFcombined' , 'meanCS.D27'),
                c('HEK', 'RFgfp' , 'percent'),
                c('HEK', 'RFcombined' , 'percent'))
colnames(out) = c('Cells','Model','Measure')
out$SpearmanCoef = NA


out$SpearmanCoef[1] = signif(cor( EG[,'standardizedGuideScores'] , EG[,'meanCS.D27'] , use = "complete.obs", method = 'spearman'),3)
out$SpearmanCoef[2] = signif(cor( EG[,'NewStandardizedGuideScores'] , EG[,'meanCS.D27'] , use = "complete.obs", method = 'spearman'),3)
out$SpearmanCoef[3] = signif(cor( EG[,'standardizedGuideScores'] , EG[,'PercentKnockdown'] , use = "complete.obs", method = 'spearman'),3)
out$SpearmanCoef[4] = signif(cor( EG[,'NewStandardizedGuideScores'] , EG[,'PercentKnockdown'] , use = "complete.obs", method = 'spearman'),3)

write.table(out , file = './data/HEK_ModelComparison_Results.txt' , col.names = T, row.names = F, quote = F)


