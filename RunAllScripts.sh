#!/bin/bash
#$ -cwd





########## step 1
# Process Metadata files needed for downstream analysis
# e.g. RNA secondary structure, RNA:RNA hybridization
# All files should be pre-computed

cd ./PreProcessingMetaData
bash PreProcess.sh
cd ..

########## step 2
# Process Tiling Screens
# Each run will produce a set of QC processing figures
# output are: 4 count-matrices (raw, normalized, batch-corrected and processed) and final crRNA enrichments 

module purge
module load R/3.5.1

cd ./TilingScreens

mkdir -p ./figures/GFP_QC
mkdir -p ./figures/CD46_QC
mkdir -p ./figures/CD55_QC
mkdir -p ./figures/CD71_QC

Rscript ./scripts/ProcessData_GFP.R     # produces Figures 1b, S2b-d, S3a-b, S3d-e
mv ./figures/*QC_*GFP*pdf ./figures/GFP_QC/
Rscript ./scripts/ProcessData_CD46.R 	# Figures S9d
mv ./figures/QC_CD46*pdf ./figures/CD46_QC/
Rscript ./scripts/ProcessData_CD55.R 	# Figures S9d
mv ./figures/QC_CD55*pdf ./figures/CD55_QC/
Rscript ./scripts/ProcessData_CD71.R 	# Figures S9d
mv ./figures/QC_CD71*pdf ./figures/CD71_QC/

cd ..

########## step 3

SCRIPTS="./scripts/"
FILES="./data/"

# Combine Tiling Screen Results into a single table used by most scripts below (./data/CombinedTilingScreenResults.csv)
# Depends on data 1
Rscript ${SCRIPTS}CombineDataSets.R

########## step 4
# basic features
Rscript ${SCRIPTS}Plot_GuideLengthEfficacy.R # FigS9e
Rscript ${SCRIPTS}Plot_GuideScoreDistributionPerGene.R 	# Fig1d, Fig 3abc, FigS9h
Rscript ${SCRIPTS}Plot_GuidePoistioningRelToSplicesites.R # FigS9f 


########## step 5
Rscript ${SCRIPTS}Plot_GuideScoreDistribution.R # produces Fig 1c, Fig S3c


############# As of here, all analysis have been done using CDS targeting guide RNAs only #####################

########## step 6
# produces FigS6a.pdf
# produces Supplementary Note 2 Figure 3
# Depends on data produced in step 1
Rscript ${SCRIPTS}Plot_RNAfoldcrRNA.R



########## step 7
# produces Supplementary Note 1 Figure 1
# produces Supplementary Note 2 Figure 1
Rscript ${SCRIPTS}Plot_antiTag.R


########## step 8
# produces Supplementary Note 1 Figure 2a
# produces Supplementary Note 2 Figure 2a
Rscript ${SCRIPTS}Plot_NTenrichment.R


########## step 9
# produces Supplementary Note 1 Figure 2b,c
# produces Supplementary Note 2 Figure 2b,c
Rscript ${SCRIPTS}Plot_NTcorrelations.R


########## step 10
# produces Fig S6a-c
# produces Supplementary Note 2 Figure 4
# Depends on data produced in step 1
Rscript ${SCRIPTS}Plot_RNAhyb.R


########## step 11
# produces Supplementary Note 1 Figure 4a and Supplementary Figure S10a-b 
Rscript ${SCRIPTS}Plot_LocalNT.R


########## step 12
# produces Supplementary Note 1 Figure 5
# produces Supplementary Note 2 Figure 5
# Depends on data produced in step 1
Rscript ${SCRIPTS}Plot_TargetSiteAccessibility.R


########## step 13
Rscript ${SCRIPTS}Plot_Mismatches_GFPscreen.R #produces Fig 1f-g and FigS4a-b
Rscript ${SCRIPTS}Plot_Mismatches_AllScreens.R



########## step 14
# Considers only GFP screen
Rscript ${SCRIPTS}Plot_CompensatoryNTcontext.R #produces FigS5a-b


########## step 15
# Normalized Fold Changes
# Produces NormalizedCombinedTilingScreenResults.csv with selected guides for modelling
Rscript ${SCRIPTS}NormalizeFoldChanges.R




########## step 16
# Porduces tables needed for model building
# The table for the initial GFP model will only be available as pre-computed files (see step 17)
Rscript ${SCRIPTS}CollectFeatures.R



########## step 17
# add here the original GFP-based model
cd PredictiveModel_GFP
bash RunModelEvaluation.sh #FigS7a-d
# Compare initial Models
Rscript ./scripts/CompareModels.R #Fig2a
# Generate final on-target Model on GFP screen only
Rscript ./scripts/Generate_final_RandomForestModel.R ./data/GFPscreen_results_PerfectMatch.csv #Fig S7e-f

cd ..


########## step 18
# Test GFP model performance in a HEK293FT cell and A375 cell essentiality Screens
cd HEKessentialityScreen
Rscript ./scripts/ProcessData_HEKessentiality.R #Fig2c and FigS8b
cd ..

cd A375essentialityScreen
Rscript ./scripts/ProcessData_A375screen.R #Fig2d-f and FigS8d
cd ..



########## step 19
# Bootstrapping - Update Model using all 4 screens
# the model input files are computed in CollectFeatures.R
ln -s  ../../data/CompleteFeatureSet.csv ./PredictiveModel_AllScreens/data/CompleteFeatureSet.csv

cd PredictiveModel_AllScreens
# Generates FigS10c here named Model_Performance_spearman_v3
bash RunModelEvaluation.sh # Inside this script you can choose different evaluation versions. Currently assigned is the v3 with the best performance (v3 == final). 



########## step 20
# Evaluate the final model using 10 fold cross-validation and leave-one-out cross-validation
# Produces Figures 3.
Rscript ./scripts/RandomForestTune.R ./data/CompleteFeatureSet.csv normCS final ./data/formulas.txt # this will take several hours. The reulst are ntree = 2000 and mtry = 12. However, these differences are marginal
Rscript ./scripts/Generate_final_RandomForestModel.R ./data/CompleteFeatureSet.csv normCS final ./data/formulas.txt # Fig3c (TenFoldCrossvalidation_Quartiles_normCS_final.pdf), FigS10e (LeaveOneOutCV_BarPlot_normCS_final.pdf) , FigS10d (TenFoldCrossvalidation_Scores_normCS_final.pdf)
Rscript ./scripts/CompareModels.R  # Fig3b
cd ..




########## step 23
# Compare performance of RFcombined to initial RFgfp model
# Comparing predictive value for the HEK293 essentiality screen
ln -s  ../../data/LetterFreq.csv ./Cas13designGuidePredictor/data/Cas13designGuidePredictorInput.csv # this contains all features needed
cp ./data/LocalNTdensityCorrelations.txt ./Cas13designGuidePredictor/data/

# Predictions for HEK essentiality screen based on the updated model have been precomputed
# cd Cas13designGuidePredictor
# bash qsub_MakePredictions.sh HEK_TargetGenes
# mv *csv ./HEK_TargetGenes/
# mv *pdf ./HEK_TargetGenes/
# rm -rf *.fa
# mv ./HEK_TargetGenes ../HEKessentialityScreen/data/TargetGenes

# bash qsub_MakePredictions.sh A375_TargetGenes
# mv *csv ./A375_TargetGenes/
# mv *pdf ./A375_TargetGenes/
# rm -rf *.fa
# mv ./A375_TargetGenes ../A375essentialityScreen/data/TargetGenes
# cd ..

cd HEKessentialityScreen
Rscript ./scripts/CompareModelsOnEssentialityScore.R
cd ..

cd A375essentialityScreen
Rscript ./scripts/CompareModelsOnEssentialityScore.R
cd ..

cd PredictiveModel_AllScreens
Rscript ./scripts/ModelComparison_EssentialityScreens.R # Fig3d, depends on the two Rscripts above
cd ..




########## step 24
# Predict guides Transcriptome-wide

cd TranscriptomeWidePredictions
bash ./scripts/GetAllTranscripts 	# downloads GENCODEv19 transcript fasta files and separates them out into sub directories 
bash ./scripts/SubmutArrayJobs.sh 	# Runs ~95000 Guide prediction jobs using SGE array submission (will take some time)
bash ./scripts/CountOutput.sh		# output generated for 94873 of 95074 GENCODE v19 proteincoding transcripts

# combine all prediction into one single data frame, selecting the top 10 guides per 5'utr/cds/3'utr per transcript
# Fig S11a-c
Rscript ./scripts/GetFinalcrRNAs.R # echo "Rscript scripts/GetFinalcrRNAs.R" | qsub -V -cwd -j y -N crRNAs -l mem=128G

cd ..







################# FACS experiments


module purge
module load R/3.5.1
Rscript ./FACS_scripts/FACS_orthologs.R # FigS1c
Rscript ./FACS_scripts/FACS_GuideLength.R # FigS1d
Rscript ./FACS_scripts/FACS_GFPScreenValidation.R # Fig1e
Rscript ./FACS_scripts/FACS_Seed.R # Fig1h
Rscript ./FACS_scripts/FACS_GFPModelConfirmation_CD46_CD71.R # Fig 2b
Rscript ./FACS_scripts/FACS_DirectRepeats.R #Supplemenatry Figure 6c
Rscript ./FACS_scripts/FACS_DRimprovement.R #Supplemenatry Figure 6d 
Rscript ./FACS_scripts/FACS_upstreamUcontext.R #Supplementary Note 1 Figure 4b-c
Rscript ./FACS_scripts/FACS_CD46ScreenValidation.R # Supplementary FigS9c









